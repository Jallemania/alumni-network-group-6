﻿using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Models.DTOs;
using Alumni_Network_BackEnd.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Alumni_Network_BackEnd.Controllers
{
    [Route("api/group")]
    [ApiController]
    [Authorize]
    public class GroupController : ControllerBase
    {

        private readonly IGroupService _groupService;
        private readonly IMapper _mapper;

        public GroupController(IGroupService groupService, IMapper mapper)
        {
            _groupService = groupService;
            _mapper = mapper;
        }

        [HttpGet("allgroups")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroups()
        {
            return _mapper.Map<List<GroupReadDTO>>(await _groupService.GetAllGroups());
        }

        [HttpGet("getallgroupsnotprivate")]

        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetAllGroupsNotPrivate()
        {
            return _mapper.Map<List<GroupReadDTO>>(await _groupService.GetAllGroupsNotPrivate());
        }

        [HttpGet("groupname")]
        public async Task<ActionResult<IEnumerable<GroupReadDTO>>> GetFetchedGroup(string groupName)
        {
            return _mapper.Map<List<GroupReadDTO>>(await _groupService.GetFetchedGroup(groupName));
        }

        [HttpGet("{groupId}")]
        public async Task<ActionResult<GroupReadDTO>> GetGroupById(int groupId)
        {
            Group group = await _groupService.GetGroupById(groupId);
            if (group == null)
            {
                return NotFound();
            }
            return _mapper.Map<GroupReadDTO>(group);
        }

        [HttpPost("{userId}")]
        public async Task<ActionResult<GroupReadDTO>> CreateGroup(GroupCreateDTO groupCreateDTO, int userId)
        {

            Group group = _mapper.Map<Group>(groupCreateDTO);

            group = await _groupService.CreateGroup(group, userId);
            return CreatedAtAction("GetGroupById", new { groupId = group.Id }, _mapper.Map<GroupReadDTO>(group));
        }

        [HttpPost("{groupId}/join")]
        public async Task<ActionResult<GroupReadDTO>> JoinGroup(int groupId, int userId)
        {
            var group = new Group();
            try
            {
                group = await _groupService.JoinGroup(groupId, userId);
            }
            catch (Exception e)
            {

                throw e;
            }

            return Ok(group);
        }

        [HttpPut("{groupId}/leave/{userId}")]
        public async Task<ActionResult<GroupReadDTO>> LeaveGroup(int groupId, int userId)
        {
            var group = new Group();
            try
            {
                group = await _groupService.LeaveGroup(groupId, userId);
            }
            catch (Exception e)
            {

                throw e;
            }

            return Ok(group);
        }
    }
}
