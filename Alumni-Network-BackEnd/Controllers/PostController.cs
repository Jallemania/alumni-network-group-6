﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Alumni_Network_BackEnd.DataAccess;
using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Models.DTOs;
using AutoMapper;
using Alumni_Network_BackEnd.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;

namespace Alumni_Network_BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostController : ControllerBase
    {


        private readonly IMapper _mapper;
        private readonly IPostService _postService;
        public PostController(IMapper mapper, IPostService postService)
        {
            _mapper = mapper;
            _postService = postService;
        }


        // GET: api/Posts
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ReadPostDTO>>> GetPosts()
        {
            return _mapper.Map<List<ReadPostDTO>>(await _postService.GetAllPostAsync());
        }

        // GET: api/Posts/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ReadPostDTO>> GetPost(int id)
        {
            Post post = await _postService.GetSpecificPostAsync(id);
            if (post == null)
            {
                return NotFound();
            }
            return _mapper.Map<ReadPostDTO>(post);

        }

        // PUT: api/Posts/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut]
        public async Task<IActionResult> PutPost(UpdatePostDTO updatePostDTO)
        {
            var post = await _postService.UpdatePostAsync(updatePostDTO);

            return CreatedAtAction("GetPost", new { id = post.Id }, post);

        }

        // POST: api/Posts

        [HttpPost]
        public async Task<IActionResult> PostPost(CreatePostDTO postDTO)
        {
            Post newPost = _mapper.Map<Post>(postDTO);

            await _postService.CreatePostAsync(newPost);
            return CreatedAtAction("GetPost", new { id = newPost.Id }, _mapper.Map<ReadPostDTO>(newPost));

        }

        private bool PostExists(int id)
        {
            // return (_context.Posts?.Any(e => e.Id == id)).GetValueOrDefault();
            return _postService.PostExists(id);
        }
        [HttpGet("UserPost/{id}")]
        public async Task<List<ReadPostDTO>> GetAllUserPosts(int id)
        {
            return await _postService.GetAllUserPosts(id);
        }
        [HttpGet("GroupPost/{id}")]
        public async Task<List<ReadPostDTO>> GetSubscribedGroupPosts(int id)
        {

            return await _postService.GetSubscribedGroupPosts(id);
        }
        [HttpGet("Post/user/{id}")]
        public async Task<List<ReadPostDTO>> GetAllUserTaggedPosts(int id)
        {

            return await _postService.GetAllUserTaggedPosts(id);
        }
        [HttpGet("Post/group/{id}")]
        public async Task<List<ReadPostDTO>> GetAllPostsFromGroup(int id)
        {

            return await _postService.GetAllPostsFromGroup(id);
        }
    }
}
