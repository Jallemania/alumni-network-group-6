﻿using Alumni_Network_BackEnd.Models.DTOs;
using Alumni_Network_BackEnd.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Alumni_Network_BackEnd.Controllers
{
    [Route("api/user")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;

        public UserController(IUserService userService)
        {
            _userService = userService;
        }


        // GET: api/user
        [HttpGet]
        public async Task<ICollection<ViewUserDTO>> GetAllUsers()
        {
            return await _userService.GetAll();
        }

        // GET api/user/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ViewUserDTO>> GetUserById(int id)
        {
            var user = await _userService.GetById(id);

            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }
        
        // GET api/user/mail/email@email.com
        [HttpGet("mail/{mail}")]
        public async Task<ActionResult<ViewUserDTO>> GetUserByMail(string mail)
        {
            var user = await _userService.GetByMail(mail);

            if (user != null)
                return Ok(user);
            else
                return NotFound();
        }

        // POST api/user
        [HttpPost("create")]
        public async Task<ActionResult<ViewUserDTO>> CreateUser(CreateUserDTO userDTO)
        {
            if (!await _userService.UserExists(userDTO.Email))
            {
                ViewUserDTO viewUser = await _userService.CreateUser(userDTO);

                return Ok(viewUser);
            }
            else
            {
                return BadRequest("User already exists");
            }

        }

        // PUT api/user
        [HttpPut("update")]
        public async Task<IActionResult> PutUser(UpdateUserDTO updateUserDTO)
        {
            var user = await _userService.PatchUser(updateUserDTO);

            return CreatedAtAction("GetUserById", new { id = user.Id }, user);
        }

        // GET api/user/exists/mj94@mail.mail
        [HttpGet("exists/{userMail}")]
        public async Task<ActionResult<bool>> UserExists(string userMail)
        {
            return Ok(await _userService.UserExists(userMail));
        }
    }
}
