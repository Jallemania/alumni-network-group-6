﻿using Alumni_Network_BackEnd.Models;
using Microsoft.EntityFrameworkCore;
using System.Data.Common;

namespace Alumni_Network_BackEnd.DataAccess
{
    public class AlumniNetworkDbContext : DbContext
    {
        public DbSet<Post>? Posts { get; set; }
        public DbSet<User>? Users { get; set; }
        public DbSet<Group>? Groups { get; set; }

        public AlumniNetworkDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            DbSeed.Seed(modelBuilder);

        }
    }
}
