﻿using Alumni_Network_BackEnd.Models;
using Microsoft.EntityFrameworkCore;

namespace Alumni_Network_BackEnd.DataAccess
{
    public static class DbSeed
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            //USERS
            User[] users =
            {
                new User()
                {
                    Id = 1,
                    Name = "Mattias",
                    Email = "coolmailman@email.com",
                    Bio = "Lorem ",
                    Status = "Experis",
                    FunFact = "Lorem",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 2,
                    Name = "Marcus",
                    Email = "c.oolmailman@email.com",
                    Bio = "Lorem ",
                    Status = "Experis",
                    FunFact = "Lorem",
                    Picture = "www.image.com"
                },
                 new User()
                {
                    Id = 3,
                    Name = "Elmin",
                    Email = "c.o.olmailman@email.com",
                    Bio = "Lorem ",
                    Status = "Experis",
                    FunFact = "Lorem",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 4,
                    Name = "Harry",
                    Email = "c.o.o.lmailman@email.com",
                    Bio = "Smart guy ",
                    Status = "Biotech",
                    FunFact = "none",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 5,
                    Name = "Johnny",
                    Email = "c.o.o.l.mailman@email.com",
                    Bio = "Hard rock",
                    Status = "Sweden rock",
                    FunFact = "Guitar",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 6,
                    Name = "Lena",
                    Email = "c.o.o.l.m.ailman@email.com",
                    Bio = "Hair Dresser",
                    Status = "Klippat o Klart",
                    FunFact = "bad puns conneseior",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 7,
                    Name = "Kingen",
                    Email = "c.o.o.l.m.a.ilman@email.com",
                    Bio = "King of the bar",
                    Status = "Unemployed",
                    FunFact = "isDrunk = true",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 8,
                    Name = "Bertil",
                    Email = "c.o.o.l.m.a.i.lman@email.com",
                    Bio = "Used to own sawmill",
                    Status = "Retired",
                    FunFact = "missing one thumb",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 9,
                    Name = "Margareta",
                    Email = "c.o.o.l.m.a.i.l.man@email.com",
                    Bio = "Sociolog",
                    Status = "Socialstyrelsen",
                    FunFact = "Nope",
                    Picture = "www.image.com"
                },
                new User()
                {
                    Id = 10,
                    Name = "Hanna",
                    Email = "c.o.o.l.m.a.i.l.m.an@email.com",
                    Bio = "Elite Fotball player",
                    Status = "Uniflex",
                    FunFact = "Has to work part time to make ends meet",
                    Picture = "www.image.com"
                }

            };

            // GROUPS
            Group[] groups =
            {
                new Group()
                {
                    Id = 1,
                    Name = ".NET allstars",
                    Description = "Lorem",
                    isPrivate = false,
                },
                new Group()
                {
                    Id = 2,
                    Name = "JAVA royals",
                    Description = "Lorem",
                    isPrivate = false,
                },
                new Group()
                {
                    Id = 3,
                    Name = "Case study group 3",
                    Description = "Lorem",
                    isPrivate = true,
                },
                new Group()
                {
                    Id = 4,
                    Name = "MAUI fan club",
                    Description = "Lorem",
                    isPrivate = true,
                },
                new Group()
                {
                    Id = 5,
                    Name = "After Work Organizational Board",
                    Description = "Lorem",
                    isPrivate = true,
                }
            };

            // POSTS
            Post[] posts =
            {
                new Post()
                {
                    Id = 1,
                    AuthorId = 2,
                    PostContent = "Första inlägget",
                    TargetGroupId = 1,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 2,
                    AuthorId = 4,
                    PostContent = "Fanny at wrong table ye in. Be on easily cannot innate in lasted months on. Differed and and felicity steepest mrs age outweigh",
                    TargetUserId = 1,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 3,
                    AuthorId = 7,
                    PostContent = "Stronger unpacked felicity to of mistaken.",
                    TargetGroupId = 3,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 4,
                    AuthorId = 3,
                    PostContent = "Excuse few the remain highly feebly add people manner say. It high at my mind by roof. ",
                    TargetGroupId = 2,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 5,
                    AuthorId = 9,
                    PostContent = "No my reached suppose proceed pressed perhaps he. Eagerness it delighted pronounce repulsive furniture no.",
                    ReplyParentId = 4,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 6,
                    AuthorId = 6,
                    PostContent = "Be me shall purse my ought times. Joy years doors all would again rooms these. Solicitude announcing as to sufficient my.",
                    ReplyParentId = 3,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 7,
                    AuthorId = 9,
                    PostContent = "Wrong for never ready ham these witty him. Our compass see age uncivil matters weather forbade her minutes",
                    ReplyParentId = 1,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 8,
                    AuthorId = 3,
                    PostContent = "Year well shot deny shew come now had. Shall downs stand marry taken his for out. Do related mr account brandon an up.",
                    ReplyParentId = 6,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 9,
                    AuthorId = 1,
                    PostContent = "Am no an listening depending up believing. Enough around remove to barton agreed regret in or it. Advantage mr estimable be commanded provision.",
                    TargetUserId = 2,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 10,
                    AuthorId = 1,
                    PostContent = "Age just her rank met down way. Attended required so in cheerful an. Domestic replying she resolved him for did. Rather in lasted no within no.",
                    TargetUserId = 7,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 11,
                    AuthorId = 4,
                    PostContent = "Too horrible consider followed may differed age. An rest if more five mr of.",
                    TargetUserId = 3,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 12,
                    AuthorId = 9,
                    PostContent = "In on announcing if of comparison pianoforte projection. Maids hoped gay yet bed asked blind dried point. On abroad danger likely regret twenty edward do.",
                    TargetUserId = 8,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 13,
                    AuthorId = 8,
                    PostContent = "Otherwise concealed favourite frankness on be at dashwoods defective at. Sympathize interested simplicity at do projecting increasing terminated. As edward settle limits at in.",
                    TargetUserId = 1,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 14,
                    AuthorId = 6,
                    PostContent = "Guest it he tears aware as. Make my no cold of need. He been past in by my hard. Warmly thrown oh he common future",
                    TargetUserId = 5,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 15,
                    AuthorId = 7,
                    PostContent = "Discretion at be an so decisively especially. Exeter itself object matter if on mr in.",
                    TargetUserId = 7,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 16,
                    AuthorId = 5,
                    PostContent = "Met come add cold calm rose mile what. Tiled manor court at built by place fanny.",
                    TargetUserId = 4,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 17,
                    AuthorId = 3,
                    PostContent = "Adapted as smiling of females oh me journey exposed concern.",
                    TargetUserId = 1,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 18,
                    AuthorId = 4,
                    PostContent = "You disposal strongly quitting his endeavor two settling him. Manners ham him hearted hundred expense. Get open game him what hour more part.",
                    TargetGroupId = 3,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 19,
                    AuthorId = 9,
                    PostContent = "Unaffected in we by apartments astonished to decisively themselves. Offended ten old consider speaking.",
                    TargetGroupId = 5,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 20,
                    AuthorId = 10,
                    PostContent = "Excellent so to no sincerity smallness. Removal request delight if on he we.",
                    TargetGroupId = 4,
                    Timestamp = DateTime.Now
                },
                new Post()
                {
                    Id = 21,
                    AuthorId = 3,
                    PostContent = "Abilities or he perfectly pretended so strangers be exquisite. Oh to another chamber pleased imagine do in. Went me rank at last loud shot an draw",
                    TargetGroupId = 2,
                    Timestamp = DateTime.Now
                },
            };

            foreach (var user in users)
            {
                modelBuilder.Entity<User>().HasData(user);
            }

            foreach (var group in groups)
            {
                modelBuilder.Entity<Group>().HasData(group);
            }

            foreach (var post in posts)
            {
                modelBuilder.Entity<Post>().HasData(post);
            }

            modelBuilder.Entity<User>()
                .HasMany(p => p.Groups)
                .WithMany(m => m.Users)
                .UsingEntity<Dictionary<string, object>>(
                    "GroupMembers",
                    r => r.HasOne<Group>().WithMany().HasForeignKey("GroupId"),
                    l => l.HasOne<User>().WithMany().HasForeignKey("UserId"),
                    je =>
                    {
                        je.HasKey("UserId", "GroupId");
                        je.HasData(
                            new { UserId = 1, GroupId = 1 },
                            new { UserId = 2, GroupId = 1 },
                            new { UserId = 3, GroupId = 2 },
                            new { UserId = 4, GroupId = 4 },
                            new { UserId = 4, GroupId = 2 },
                            new { UserId = 5, GroupId = 3 },
                            new { UserId = 6, GroupId = 5 },
                            new { UserId = 6, GroupId = 4 },
                            new { UserId = 6, GroupId = 1 },
                            new { UserId = 7, GroupId = 2 },
                            new { UserId = 8, GroupId = 3 },
                            new { UserId = 9, GroupId = 2 }

                        );
                    });

            modelBuilder.Entity<Post>()
               .HasOne(p => p.Author)
               .WithMany(a => a.Posts)
               .HasForeignKey(p => p.AuthorId);

        }
    }
}
