﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Alumni_Network_BackEnd.Migrations
{
    public partial class addusermailprop : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100,
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: false,
                defaultValue: "");

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(163));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(205));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(208));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(210));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(212));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(214));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(215));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(217));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(219));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(265));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(267));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(269));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(271));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(273));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(275));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(277));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(279));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(281));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(283));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(284));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 21,
                column: "Timestamp",
                value: new DateTime(2022, 9, 9, 10, 6, 51, 448, DateTimeKind.Local).AddTicks(286));

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 1,
                column: "Email",
                value: "coolmailman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 2,
                column: "Email",
                value: "c.oolmailman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 3,
                column: "Email",
                value: "c.o.olmailman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 4,
                column: "Email",
                value: "c.o.o.lmailman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 5,
                column: "Email",
                value: "c.o.o.l.mailman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 6,
                column: "Email",
                value: "c.o.o.l.m.ailman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 7,
                column: "Email",
                value: "c.o.o.l.m.a.ilman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 8,
                column: "Email",
                value: "c.o.o.l.m.a.i.lman@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 9,
                column: "Email",
                value: "c.o.o.l.m.a.i.l.man@email.com");

            migrationBuilder.UpdateData(
                table: "Users",
                keyColumn: "Id",
                keyValue: 10,
                column: "Email",
                value: "c.o.o.l.m.a.i.l.m.an@email.com");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Email",
                table: "Users");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Users",
                type: "nvarchar(100)",
                maxLength: 100,
                nullable: true,
                oldClrType: typeof(string),
                oldType: "nvarchar(100)",
                oldMaxLength: 100);

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 1,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9761));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 2,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9807));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 3,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9810));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 4,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9813));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 5,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9815));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 6,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9818));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 7,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9820));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 8,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9823));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 9,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9825));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 10,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9828));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 11,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9830));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 12,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9833));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 13,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9835));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 14,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9838));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 15,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9840));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 16,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9843));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 17,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9845));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 18,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9847));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 19,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9850));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 20,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9852));

            migrationBuilder.UpdateData(
                table: "Posts",
                keyColumn: "Id",
                keyValue: 21,
                column: "Timestamp",
                value: new DateTime(2022, 9, 8, 10, 3, 2, 925, DateTimeKind.Local).AddTicks(9855));
        }
    }
}
