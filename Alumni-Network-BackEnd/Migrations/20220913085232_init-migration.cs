﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Alumni_Network_BackEnd.Migrations
{
    public partial class initmigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Description = table.Column<string>(type: "nvarchar(300)", maxLength: 300, nullable: true),
                    isPrivate = table.Column<bool>(type: "bit", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: true),
                    Status = table.Column<string>(type: "nvarchar(250)", maxLength: 250, nullable: true),
                    Picture = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true),
                    Bio = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    FunFact = table.Column<string>(type: "nvarchar(200)", maxLength: 200, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "GroupMembers",
                columns: table => new
                {
                    UserId = table.Column<int>(type: "int", nullable: false),
                    GroupId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupMembers", x => new { x.UserId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_GroupMembers_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GroupMembers_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Posts",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Timestamp = table.Column<DateTime>(type: "datetime2", nullable: false),
                    AuthorId = table.Column<int>(type: "int", nullable: true),
                    PostContent = table.Column<string>(type: "nvarchar(500)", maxLength: 500, nullable: true),
                    ReplyParentId = table.Column<int>(type: "int", nullable: true),
                    TargetUserId = table.Column<int>(type: "int", nullable: true),
                    TargetGroupId = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Posts", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Posts_Groups_TargetGroupId",
                        column: x => x.TargetGroupId,
                        principalTable: "Groups",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Posts_ReplyParentId",
                        column: x => x.ReplyParentId,
                        principalTable: "Posts",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Users_AuthorId",
                        column: x => x.AuthorId,
                        principalTable: "Users",
                        principalColumn: "Id");
                    table.ForeignKey(
                        name: "FK_Posts_Users_TargetUserId",
                        column: x => x.TargetUserId,
                        principalTable: "Users",
                        principalColumn: "Id");
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "Description", "Name", "isPrivate" },
                values: new object[,]
                {
                    { 1, "Lorem", ".NET allstars", false },
                    { 2, "Lorem", "JAVA royals", false },
                    { 3, "Lorem", "Case study group 3", true },
                    { 4, "Lorem", "MAUI fan club", true },
                    { 5, "Lorem", "After Work Organizational Board", true }
                });

            migrationBuilder.InsertData(
                table: "Users",
                columns: new[] { "Id", "Bio", "FunFact", "Name", "Picture", "Status" },
                values: new object[,]
                {
                    { 1, "Lorem ", "Lorem", "Mattias", "www.image.com", "Experis" },
                    { 2, "Lorem ", "Lorem", "Marcus", "www.image.com", "Experis" },
                    { 3, "Lorem ", "Lorem", "Elmin", "www.image.com", "Experis" },
                    { 4, "Smart guy ", "none", "Harry", "www.image.com", "Biotech" },
                    { 5, "Hard rock", "Guitar", "Johnny", "www.image.com", "Sweden rock" },
                    { 6, "Hair Dresser", "bad puns conneseior", "Lena", "www.image.com", "Klippat o Klart" },
                    { 7, "King of the bar", "isDrunk = true", "Kingen", "www.image.com", "Unemployed" },
                    { 8, "Used to own sawmill", "missing one thumb", "Bertil", "www.image.com", "Retired" },
                    { 9, "Sociolog", "Nope", "Margareta", "www.image.com", "Socialstyrelsen" },
                    { 10, "Elite Fotball player", "Has to work part time to make ends meet", "Hanna", "www.image.com", "Uniflex" }
                });

            migrationBuilder.InsertData(
                table: "GroupMembers",
                columns: new[] { "GroupId", "UserId" },
                values: new object[,]
                {
                    { 1, 1 },
                    { 1, 2 },
                    { 2, 3 },
                    { 2, 4 },
                    { 4, 4 },
                    { 3, 5 },
                    { 1, 6 },
                    { 4, 6 },
                    { 5, 6 },
                    { 2, 7 },
                    { 3, 8 },
                    { 2, 9 }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "PostContent", "ReplyParentId", "TargetGroupId", "TargetUserId", "Timestamp" },
                values: new object[,]
                {
                    { 1, 2, "Första inlägget", null, 1, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2124) },
                    { 2, 4, "Fanny at wrong table ye in. Be on easily cannot innate in lasted months on. Differed and and felicity steepest mrs age outweigh", null, null, 1, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2166) },
                    { 3, 7, "Stronger unpacked felicity to of mistaken.", null, 3, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2168) },
                    { 4, 3, "Excuse few the remain highly feebly add people manner say. It high at my mind by roof. ", null, 2, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2169) },
                    { 9, 1, "Am no an listening depending up believing. Enough around remove to barton agreed regret in or it. Advantage mr estimable be commanded provision.", null, null, 2, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2178) },
                    { 10, 1, "Age just her rank met down way. Attended required so in cheerful an. Domestic replying she resolved him for did. Rather in lasted no within no.", null, null, 7, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2180) },
                    { 11, 4, "Too horrible consider followed may differed age. An rest if more five mr of.", null, null, 3, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2181) },
                    { 12, 9, "In on announcing if of comparison pianoforte projection. Maids hoped gay yet bed asked blind dried point. On abroad danger likely regret twenty edward do.", null, null, 8, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2183) },
                    { 13, 8, "Otherwise concealed favourite frankness on be at dashwoods defective at. Sympathize interested simplicity at do projecting increasing terminated. As edward settle limits at in.", null, null, 1, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2185) },
                    { 14, 6, "Guest it he tears aware as. Make my no cold of need. He been past in by my hard. Warmly thrown oh he common future", null, null, 5, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2186) },
                    { 15, 7, "Discretion at be an so decisively especially. Exeter itself object matter if on mr in.", null, null, 7, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2188) },
                    { 16, 5, "Met come add cold calm rose mile what. Tiled manor court at built by place fanny.", null, null, 4, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2189) },
                    { 17, 3, "Adapted as smiling of females oh me journey exposed concern.", null, null, 1, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2192) },
                    { 18, 4, "You disposal strongly quitting his endeavor two settling him. Manners ham him hearted hundred expense. Get open game him what hour more part.", null, 3, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2194) },
                    { 19, 9, "Unaffected in we by apartments astonished to decisively themselves. Offended ten old consider speaking.", null, 5, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2195) },
                    { 20, 10, "Excellent so to no sincerity smallness. Removal request delight if on he we.", null, 4, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2197) },
                    { 21, 3, "Abilities or he perfectly pretended so strangers be exquisite. Oh to another chamber pleased imagine do in. Went me rank at last loud shot an draw", null, 2, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2198) }
                });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "PostContent", "ReplyParentId", "TargetGroupId", "TargetUserId", "Timestamp" },
                values: new object[] { 5, 9, "No my reached suppose proceed pressed perhaps he. Eagerness it delighted pronounce repulsive furniture no.", 4, null, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2171) });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "PostContent", "ReplyParentId", "TargetGroupId", "TargetUserId", "Timestamp" },
                values: new object[] { 6, 6, "Be me shall purse my ought times. Joy years doors all would again rooms these. Solicitude announcing as to sufficient my.", 3, null, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2172) });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "PostContent", "ReplyParentId", "TargetGroupId", "TargetUserId", "Timestamp" },
                values: new object[] { 7, 9, "Wrong for never ready ham these witty him. Our compass see age uncivil matters weather forbade her minutes", 1, null, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2174) });

            migrationBuilder.InsertData(
                table: "Posts",
                columns: new[] { "Id", "AuthorId", "PostContent", "ReplyParentId", "TargetGroupId", "TargetUserId", "Timestamp" },
                values: new object[] { 8, 3, "Year well shot deny shew come now had. Shall downs stand marry taken his for out. Do related mr account brandon an up.", 6, null, null, new DateTime(2022, 9, 13, 10, 52, 31, 841, DateTimeKind.Local).AddTicks(2176) });

            migrationBuilder.CreateIndex(
                name: "IX_GroupMembers_GroupId",
                table: "GroupMembers",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_AuthorId",
                table: "Posts",
                column: "AuthorId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_ReplyParentId",
                table: "Posts",
                column: "ReplyParentId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TargetGroupId",
                table: "Posts",
                column: "TargetGroupId");

            migrationBuilder.CreateIndex(
                name: "IX_Posts_TargetUserId",
                table: "Posts",
                column: "TargetUserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupMembers");

            migrationBuilder.DropTable(
                name: "Posts");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
