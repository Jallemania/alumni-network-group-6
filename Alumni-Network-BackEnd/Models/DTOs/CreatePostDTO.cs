﻿using System.ComponentModel.DataAnnotations;

namespace Alumni_Network_BackEnd.Models.DTOs
{
    public class CreatePostDTO
    {
        [MaxLength(500)]
        public string? PostContent { get; set; }

        public int AuthorId { get; set; }
        public int? ReplyParentId { get; set; }

        public int? TargetUserId { get; set; }

        public int? TargetGroupId { get; set; }
      


    }
}
