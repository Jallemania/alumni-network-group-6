﻿using System.ComponentModel.DataAnnotations;

namespace Alumni_Network_BackEnd.Models.DTOs
{
    public class CreateUserDTO
    {
        [MaxLength(100)]
        public string? Name { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }

    }
}
