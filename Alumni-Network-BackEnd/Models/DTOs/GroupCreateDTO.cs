﻿namespace Alumni_Network_BackEnd.Models.DTOs
{
    public class GroupCreateDTO
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public bool? IsPrivate { get; set; }

    }
}
