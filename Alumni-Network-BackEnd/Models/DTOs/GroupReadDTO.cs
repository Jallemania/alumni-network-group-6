﻿using System.ComponentModel.DataAnnotations;

namespace Alumni_Network_BackEnd.Models.DTOs
{
    public class GroupReadDTO
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string? Name { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        public bool? isPrivate { get; set; }
        public ICollection<User>? Users { get; set; }
        public ICollection<Post>? Posts { get; set; }
    }
}
