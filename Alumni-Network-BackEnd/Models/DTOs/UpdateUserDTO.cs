﻿using System.ComponentModel.DataAnnotations;

namespace Alumni_Network_BackEnd.Models.DTOs
{
    public class UpdateUserDTO
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string? Name { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(250)]
        public string? Status { get; set; }
        [MaxLength(200)]
        public string? Picture { get; set; }
        [MaxLength(500)]
        public string? Bio { get; set; }
        [MaxLength(200)]
        public string? FunFact { get; set; }

        //public ICollection<Post>? Posts { get; set; }
        //public ICollection<Group>? Groups { get; set; }

        //public UpdateUserDTO()
        //{
        //    Posts = new List<Post>();
        //    Groups = new List<Group>();
        //}
    }
}
