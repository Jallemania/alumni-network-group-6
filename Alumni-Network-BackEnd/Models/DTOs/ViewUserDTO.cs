﻿using System.ComponentModel.DataAnnotations;

namespace Alumni_Network_BackEnd.Models.DTOs
{
    public class ViewUserDTO
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        [MaxLength(100)]
        public string Email { get; set; }
        [MaxLength(250)]
        public string? Status { get; set; }
        [MaxLength(200)]
        public string? Picture { get; set; }
        [MaxLength(500)]
        public string? Bio { get; set; }
        [MaxLength(200)]
        public string? FunFact { get; set; }

        public ICollection<Post>? Posts { get; set; }
        public ICollection<Group>? Groups { get; set; }
    }
}
