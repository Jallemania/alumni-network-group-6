﻿using System.ComponentModel.DataAnnotations;

namespace Alumni_Network_BackEnd.Models
{
    public class Group
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string? Name { get; set; }
        [MaxLength(300)]
        public string? Description { get; set; }
        public bool? isPrivate { get; set; }
        public ICollection<User>? Users { get; set; }
        public ICollection<Post>? Posts { get; set; }
        public Group()
        {
            Users = new List<User>();
            Posts = new List<Post>();
        }
    }
}
