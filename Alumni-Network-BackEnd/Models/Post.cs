﻿using System.ComponentModel.DataAnnotations;

namespace Alumni_Network_BackEnd.Models
{
    public class Post
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public int? AuthorId { get; set; }
        public User? Author { get; set; }
        [MaxLength(500)]
        public string? PostContent { get; set; }
        public int? ReplyParentId { get; set; }
        public Post? ReplyParent { get; set; }
        public int? TargetUserId { get; set; }
        public User? TargetUser { get; set; }
        public int? TargetGroupId { get; set; }
        public Group? TargetGroup { get; set; }
    }
}
