﻿using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Models.DTOs;
using AutoMapper;

namespace Alumni_Network_BackEnd.Profiles
{
    public class GroupProfile: Profile
    {
        public GroupProfile()
        {
            CreateMap<Group, GroupReadDTO>().ReverseMap();
            CreateMap<Group, GroupCreateDTO>().ReverseMap();
        }
    }
}
