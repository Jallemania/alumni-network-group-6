﻿using Alumni_Network_BackEnd.Models.DTOs;
using AutoMapper;

namespace Alumni_Network_BackEnd.Models.Profiles
{
    public class PostProfile: Profile
    {
        public PostProfile()
        {
            CreateMap<ReadPostDTO, Post>().ReverseMap();
            CreateMap<UpdatePostDTO, Post>().ReverseMap();
            CreateMap<CreatePostDTO, Post>().ReverseMap();

        }
    }
}
