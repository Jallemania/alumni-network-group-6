﻿using Alumni_Network_BackEnd.Models.DTOs;
using AutoMapper;

namespace Alumni_Network_BackEnd.Models.Profiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UpdateUserDTO, User>().ReverseMap();
            CreateMap<ViewUserDTO, User>().ReverseMap();
            CreateMap<CreateUserDTO, User>().ReverseMap();
        }
    }
}
