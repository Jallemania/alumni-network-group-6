﻿using Alumni_Network_BackEnd.DataAccess;
using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Alumni_Network_BackEnd.Services
{
    public class GroupService : IGroupService
    {
        private readonly AlumniNetworkDbContext _context;

        public GroupService(AlumniNetworkDbContext context)
        {
            _context = context;

        }
       
        public async Task<IEnumerable<Group>> GetAllGroups()
        {
            return await _context.Groups.Include(g => g.Users).ToListAsync();

        }

        public async Task<IEnumerable<Group>> GetAllGroupsNotPrivate()
        {
            return await _context.Groups.Include(g => g.Users).Where(g => g.isPrivate == false).ToListAsync();
        }

        public async Task<IEnumerable<Group>> GetFetchedGroup(string groupName)
        {
            var groups = from g in _context.Groups select g;

            if (!String.IsNullOrEmpty(groupName))
            {
                groups = groups.Where(s => s.Name.ToLower() == groupName.ToLower());
            }
            return await groups.ToListAsync();
        }

        public async Task<Group> GetGroupById(int groupId)
        {
            var group = await _context.Groups.Include(g => g.Users).Include(g => g.Posts).FirstOrDefaultAsync(g => g.Id == groupId);

            if (group.Posts.Count() > 0)
            {
                foreach (var post in group.Posts)
                {
                    post.Author = null;
                }
            }

            if (group.Users.Count() > 0)
            {
                foreach (var user in group.Users)
                {
                    user.Groups = null;
                    user.Posts = null;
                }
            }
            return group;
        }

        public async Task<Group> CreateGroup(Group newGroup, int userId)
        {
            var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == userId);
            newGroup.Users.Add(user);

            await _context.Groups.AddAsync(newGroup);

            if (_context.SaveChanges() == 0)
                throw new Exception("Hey Hey");

            return newGroup;

        }
        public async Task<Group> JoinGroup(int groupId, int userId)
        {
            var group = await _context.Groups.Include(u => u.Users).SingleOrDefaultAsync(g => g.Id == groupId);

            var user = await _context.Users.SingleOrDefaultAsync(u => u.Id == userId);

            if (!group.Users.Any(u => u.Id == userId) && user is not null)
            {
                group.Users.Add(user);
            }
            else
            {
                throw new Exception("User is already a meber");
            }

            _context.Groups.Update(group);

            if (_context.SaveChanges() == 0)
                throw new Exception("Hey Hey");

            return group;
        }

        public async Task<Group> LeaveGroup(int groupId, int userId)
        {

            var user = await _context.Users.FindAsync(userId);

            if (user == null)
            {
                throw new Exception("User not found");
            }

            var group = await _context.Groups.Include(u => u.Users).SingleOrDefaultAsync(g => g.Id == groupId);

            try
            {
                group.Users.Where(u => u.Id == userId).ToList().ForEach(u => group.Users.Remove(u));
                _context.Groups.Update(group);
                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                throw;
            }

            return group;
        }
    }
}
