﻿using Alumni_Network_BackEnd.Models;

namespace Alumni_Network_BackEnd.Services.Interfaces
{
    public interface IGroupService
    {   
        /// <summary>
        /// Get all groups from the database
        /// </summary>
        /// <returns>Returns all groups</returns>
        public Task<IEnumerable<Group>> GetAllGroups();

        /// <summary>
        /// Get all groups from database that are not private
        /// </summary>
        /// <returns>Returns all groups that are not private</returns>
        public Task<IEnumerable<Group>> GetAllGroupsNotPrivate();

        /// <summary>
        /// Fetchs groups by name
        /// </summary>
        /// <param name="groupName"> string parameter</param>
        /// <returns>Returns the group</returns>
        public Task<IEnumerable<Group>> GetFetchedGroup(string groupName);
        
        /// <summary>
        /// Get group by id
        /// </summary>
        /// <param name="groupId"></param>
        /// <returns>Returns specific group</returns>
        public Task<Group> GetGroupById(int groupId);

        /// <summary>
        /// Creates a group with userid as firstmember
        /// </summary>
        /// <param name="group">Group</param>
        /// <param name="userId">specific userId</param>
        /// <returns>Creates a new group with the specific user as the first member</returns>
        public Task<Group> CreateGroup(Group group, int userId);

        /// <summary>
        /// User can join a specif group
        /// </summary>
        /// <param name="groupId">specific groupId</param>
        /// <param name="userId">specific userId</param>
        /// <returns>User joins a specific group</returns>
        public Task<Group> JoinGroup(int groupId, int userId);


        /// <summary>
        /// A user can leave a specific group
        /// </summary>
        /// <param name="groupId">specific groupId</param>
        /// <param name="userId">specific userId</param>
        /// <returns>returns a updated group </returns>
        public Task<Group> LeaveGroup(int groupId, int userId);

  
    }
}
