﻿using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Models.DTOs;

namespace Alumni_Network_BackEnd.Services.Interfaces
{
    public interface IPostService
    {
        /// <summary>
        /// Gets all the posts in the database
        /// </summary>
        /// <returns>A list of all posts</returns>
        public Task<IEnumerable<Post>> GetAllPostAsync(); 
        /// <summary>
        /// Get one specific post by ID
        /// </summary>
        /// <param name="id">The id of the desired post</param>
        /// <returns>return the specific post</returns>
        public Task<Post> GetSpecificPostAsync(int id);
        /// <summary>
        /// Creates a new post and sends it to the database
        /// </summary>
        /// <param name="post">Post object</param>
        /// <returns>No Return</returns>
        public Task<Post> CreatePostAsync(Post post);
        /// <summary>
        /// Updates a post
        /// </summary>
        /// <param name="post">Post object</param>
        /// <returns>No Return</returns>
        public Task<ReadPostDTO> UpdatePostAsync(UpdatePostDTO postDTO);
        /// <summary>
        /// Gets all posts were the user is the author
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>Returns a list of the users posts</returns>
        public Task<List<ReadPostDTO>> GetAllUserPosts(int id);
        /// <summary>
        /// Gets all posts from one specific group
        /// </summary>
        /// <param name="groupId">The Id of the group</param>
       
        /// <returns>Return a list of the groups posts</returns>
        public Task<List<ReadPostDTO>> GetAllPostsFromGroup(int id);
    
        /// <summary>
        /// Checks if the exists
        /// </summary>
        /// <param name="id">Id of the post</param>
        /// <returns>Returns Boolean</returns>
        public bool PostExists(int id);
        /// <summary>
        /// Gets all posts from all the groups the user is in
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>Returns a list of all posts in the subscribed groups</returns>
        public Task<List<ReadPostDTO>> GetSubscribedGroupPosts(int id);
        /// <summary>
        /// Gets all the posts with the user as targetUser
        /// </summary>
        /// <param name="id">Id of the user</param>
        /// <returns>Returns a list of all users</returns>
        public Task<List<ReadPostDTO>> GetAllUserTaggedPosts(int id);

    }
}
