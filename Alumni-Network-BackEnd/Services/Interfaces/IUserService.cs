﻿using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Models.DTOs;

namespace Alumni_Network_BackEnd.Services.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Takes in an Id and returns the user referenced by that Id.
        /// </summary>
        /// <param name="id">User Id</param>
        /// <returns>ViewUserDTO</returns>
        public Task<ViewUserDTO> GetById(int id);

        /// <summary>
        /// Gets all the users in the database and returns them in a list.
        /// </summary>
        /// <returns>List with ViewUserDTO of all users in database</returns>
        public Task<ICollection<ViewUserDTO>> GetAll();

        /// <summary>
        /// Takes in an updated user DTO and applies the changes to the database.
        /// </summary>
        /// <param name="updatedUser">A user DTO with updated properties</param>
        /// <returns>An updated ViewUserDTO</returns>
        public Task<ViewUserDTO> PatchUser(UpdateUserDTO updatedUser);

        /// <summary>
        /// Takes in a user id and checks if user exists in database.
        /// </summary>
        /// <param name="userId">id of user database entity.</param>
        /// <returns>A bool based on user existance.</returns>
        public Task<bool> UserExists(string userMail);

        /// <summary>
        /// Takes in a string Mail and returns the user referenced by that mail.
        /// </summary>
        /// <param name="id">User Email</param>
        /// <returns>ViewUserDTO</returns>
        public Task<ViewUserDTO> GetByMail(string userMail);

        /// <summary>
        /// Takes in a DTO and converts it to a user object and then adds it to the database.
        /// </summary>
        /// <param name="userDTO">DTO of a user object.</param>
        /// <returns>a ViewDTO for a user.</returns>
        public Task<ViewUserDTO> CreateUser(CreateUserDTO userDTO);
    }
}
