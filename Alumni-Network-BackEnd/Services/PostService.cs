﻿using Alumni_Network_BackEnd.DataAccess;
using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Models.DTOs;
using Alumni_Network_BackEnd.Services.Interfaces;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;

namespace Alumni_Network_BackEnd.Services
{
    public class PostService : IPostService
    {
        private readonly AlumniNetworkDbContext _context;
        private readonly IMapper _mapper;
        public PostService(AlumniNetworkDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;

        }
        public async Task<Post> CreatePostAsync(Post post)
        {
            post.Timestamp = DateTime.Now;
            _context.Posts.Add(post);
            await _context.SaveChangesAsync();
            return post;
        }

        public async Task<IEnumerable<Post>> GetAllPostAsync()
        {

            return await _context.Posts.ToListAsync();
        }

        public async Task<List<ReadPostDTO>> GetAllUserPosts(int id)
        {
            var allPosts = await _context.Posts.ToListAsync();

            List<ReadPostDTO> postList = new();

            if (allPosts != null)
            {

                foreach (var post in allPosts)
                {

                    if (post.AuthorId == id)
                    {

                        postList.Add(_mapper.Map<ReadPostDTO>(post));
                    }

                }
            }
            return postList;

        }

        public async Task<Post> GetSpecificPostAsync(int id)
        {
            return _context.Posts.SingleOrDefault(f => f.Id == id);
        }

        public bool PostExists(int id)
        {
            return _context.Posts.Any(e => e.Id == id);
        }

        public async Task<ReadPostDTO> UpdatePostAsync(UpdatePostDTO postDTO)
        {

            Post oldPost = await _context.Posts.SingleOrDefaultAsync(p => p.Id == postDTO.Id);

            if (oldPost != null)
            {
                _mapper.Map(postDTO, oldPost);
                _context.Posts.Update(oldPost);
            }

            if (_context.SaveChangesAsync().Result > 0)
                return _mapper.Map<ReadPostDTO>(oldPost);
            else
                throw new Exception();
        }

        public async Task<List<ReadPostDTO>> GetAllPostsFromGroup(int id)
        {
            // var group = await _context.Groups.Include(g => g.Posts).SingleOrDefaultAsync(g => g.Id == id);
            var posts = await _context.Posts.ToListAsync();
            List<ReadPostDTO> postList = new();


            foreach (var post in posts)
            {
                if (post.TargetGroupId == id)
                {
                    postList.Add(_mapper.Map<ReadPostDTO>(post));
                }
            }


            return postList;

        }

        //}
        public async Task<List<ReadPostDTO>> GetSubscribedGroupPosts(int id)
        {
            //var groups = await _context.Groups.Include(g => g.Posts).Include(g => g.Users).ToListAsync(); //Hämtar alla grupper
            var user = await _context.Users.Include(g => g.Groups).ThenInclude(g => g.Posts).SingleOrDefaultAsync(g => g.Id == id);
            List<ReadPostDTO> postList = new();


            foreach (var group in user.Groups)
            {
                foreach (var post in group.Posts)
                {
                    postList.Add(_mapper.Map<ReadPostDTO>(post));
                }
            }
            return postList;

        }

        public async Task<List<ReadPostDTO>> GetAllUserTaggedPosts(int id)
        {
            var posts = await _context.Posts.ToListAsync();
            //var groups = await _context.Groups.Include(g => g.Posts).Include(g => g.Users).ToListAsync(); //Hämtar alla grupper
            //var user = await _context.Users.Include(g => g.Groups).ThenInclude(g => g.Posts).SingleOrDefaultAsync(g => g.Id == userId);
            List<ReadPostDTO> postList = new();


            foreach (var post in posts)
            {
                if (post.TargetUserId == id)
                {
                    postList.Add(_mapper.Map<ReadPostDTO>(post));
                }
            }
            return postList;

        }
    }
}
