﻿using Alumni_Network_BackEnd.DataAccess;
using Alumni_Network_BackEnd.Models;
using Alumni_Network_BackEnd.Models.DTOs;
using Alumni_Network_BackEnd.Services.Interfaces;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace Alumni_Network_BackEnd.Services
{
    public class UserService : IUserService
    {
        private readonly AlumniNetworkDbContext _context;
        private readonly IMapper _mapper;

        public UserService(AlumniNetworkDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public async Task<ViewUserDTO> CreateUser(CreateUserDTO userDTO)
        {
            User newUser = _mapper.Map<User>(userDTO);

            await _context.Users.AddAsync(newUser);

            if (_context.SaveChanges() == 0)
                throw new Exception("User not added to DB");

            return _mapper.Map<ViewUserDTO>(newUser);
        }

        public async Task<ICollection<ViewUserDTO>> GetAll()
        {
            List<ViewUserDTO> usersDTO = new();
            var usersDB = await _context.Users.ToListAsync();

            foreach (var user in usersDB)
            {
                usersDTO.Add(_mapper.Map<ViewUserDTO>(user));
            }

            return usersDTO;
        }

        public async Task<ViewUserDTO> GetById(int id)
        {
            ViewUserDTO userDTO = _mapper.Map<ViewUserDTO>(await _context.Users
                .Include(u => u.Posts)
                .Include(u => u.Groups)
                .SingleOrDefaultAsync(u => u.Id == id));


            if (userDTO.Groups.Count() > 0)
            {
                foreach (var group in userDTO.Groups)
                {
                    group.Users = null;
                    group.Posts = null;
                }
            }

            if (userDTO.Posts.Count() > 0)
            {
                foreach (Post post in userDTO.Posts)
                {
                    post.Author = null;
                }
            }

            return userDTO;
        }

        public async Task<ViewUserDTO> GetByMail(string userMail)
        {
            ViewUserDTO userDTO = _mapper.Map<ViewUserDTO>(await _context.Users
                .Include(u => u.Posts)
                .Include(u => u.Groups)
                .SingleOrDefaultAsync(u => u.Email == userMail));


            if (userDTO.Groups.Count() > 0)
            {
                foreach (var group in userDTO.Groups)
                {
                    group.Users = null;
                    group.Posts = null;
                }
            }

            if (userDTO.Posts.Count() > 0)
            {
                foreach (Post post in userDTO.Posts)
                {
                    post.Author = null;
                }
            }

            return userDTO;
        }

        public async Task<ViewUserDTO> PatchUser(UpdateUserDTO updatedUser)
        {
            User oldUser = await _context.Users.SingleOrDefaultAsync(u => u.Id == updatedUser.Id);

            if (oldUser != null)
            {
                _mapper.Map(updatedUser, oldUser);
                _context.Users.Update(oldUser);
            }

            if (_context.SaveChangesAsync().Result > 0)
                return _mapper.Map<ViewUserDTO>(oldUser);
            else
                throw new Exception();


        }

        public async Task<bool> UserExists(string userMail)
        {
            return await _context.Users.AnyAsync(u => u.Email == userMail);
        }

    }
}
