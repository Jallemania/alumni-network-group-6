import "./App.css";
import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { ROLES } from "./const/roles";
import LandingPage from "./Components/LandingPageComponent/LandingPage";
import ProfilePage from "./Components/UserPageComponent/ProfilePage";
import UserPage from "./Components/UserPageComponent/UserPage";
import RoleRoute from "./AuthRoute/RoleRoute";
import Timeline from "./Components/TimelineComponent/Timeline";
import Navbar from "./Components/NavbarComponent/Navbar";
import Group from "./Components/GroupPageComponent/Group";
import { lazy, Suspense  } from "react";

const GroupsPage = lazy(() => import ('./Components/GroupPageComponent/GroupPage'));

function App() {

  
  return (
    <BrowserRouter>
      <div className="App text-wrap">
        <Navbar/>
        <div className="mb-5 pb-5 pt-5"></div>
          <Suspense fallback={<div>Loading...</div>}>
        <Routes>
          <Route path="/" element={<LandingPage />} />
          <Route
            path="/timeline"
            element={
              <RoleRoute role={ROLES.User} redirectTo="/">
                <Timeline />
              </RoleRoute>
            }
          />

          <Route
            path="/groups"
            element={
              <RoleRoute role={ROLES.User} redirectTo="/">
                <GroupsPage />
              </RoleRoute>
            }
          />
          <Route
            path="/groups/viewGroup"
            element={
              <RoleRoute role={ROLES.User} redirectTo="/">
                <Group/>
              </RoleRoute>
            }
          />  
          <Route
            path="/profile"
            element={
              <RoleRoute role={ROLES.User} redirectTo="/">
                <ProfilePage />
              </RoleRoute>
            }
          />
          <Route
            path="/user"
            element={
              <RoleRoute role={ROLES.User} redirectTo="/">
                <UserPage />
              </RoleRoute>
            }
          />
          
        </Routes>
        </Suspense>
      </div>
    </BrowserRouter>
  );
}

export default App;
