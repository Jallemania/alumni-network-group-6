import keycloak from "../keycloak/keycloak";
import { NavLink } from 'react-router-dom';

function AuthRoute( { children } ){
    if (keycloak.authenticated ){ // Logged in
        return(
            <div>{children}</div>
        )
    } 
    
    else { // Not logged in
        return(
            <div>
                <p>You must be logged in to view this page</p>
                <NavLink className="btn btn-primary" to="/">Return Home</NavLink>
            </div>
        )
    }
}

export default AuthRoute;
