import keycloak from "../keycloak/keycloak";
import { Navigate, NavLink } from 'react-router-dom';

import React from 'react'

const RoleRoute = ({ children, role }) => {
   if(!keycloak.authenticated){
        return (
            <div>
                <p>You must be logged in to view this page</p>
                <NavLink to="/">
                    <button className="primary-btn">Return Home</button>
                </NavLink>
            </div>
        )
    }

    if(keycloak.hasResourceRole(role)){
        return <div>{children}</div>;
    }

    return <Navigate replace to="/"/>

}

export default RoleRoute