import { useEffect } from "react";
import { useState } from "react";
import { PostPost } from "../../api/apiPost";
import { getUserByMail } from "../../api/userApi";
import keycloak from "../../keycloak/keycloak";

const CreatePost = ({ users, groups, handleToplevelPosts, postArray, setPostArray }) => {
  const [userArray] = useState(users);
  const [groupArray] = useState(groups);
  const [user, setUser] = useState()
  const [postContent, setPostContent] = useState()
  const [targetGroup, setTargetGroup] = useState()
  const [targetAuthor, setTargetAuthor] = useState()

  useEffect(() => {
    getUserByMail(keycloak.tokenParsed.email).then(res => setUser(res))
  }, [])

    const handleSubmit = async (e) => {
        e.preventDefault()

        let post = {
            postContent: postContent,
            authorId: user.id,
            targetGroupId: targetGroup,
            targetAuthorId: targetAuthor,
            replyParentId: null
        }
        const test = await PostPost(post);

        setPostArray(state => [...state, test])
        handleToplevelPosts()

        setPostContent('')
        setTargetAuthor('')
        setTargetGroup('')
    }


  return (
    <div className="w-50">
      <form onSubmit={handleSubmit}>
        <div className="mb-3">
          <label htmlFor="postContent" className="form-label">
            New Post
          </label>
          <textarea
            type="text"
            className="form-control pb-5"
            value={postContent}
            id="postContent"
            aria-describedby="postHelp"
            onChange={(e) => setPostContent(e.target.value)}
          />
          <div id="postHelp" className="form-text">
            What's on your mind today?
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="targetGroup" className="form-label">
            Post in Group:
          </label>
          <select
            className="form-select"
            aria-label="default select example"
            value={targetGroup}
            name='targetGroup'
            id="targetGroup"
            onChange={(e) => setTargetGroup(e.target.value)}
          >
            <option defaultValue={null}>Which Group</option>
            {groupArray?.map((g) => {
              return (
                <option key={g.id} value={g.id}>
                  {g.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="mb-3">
          <label htmlFor="targetAuthor" className="form-label">
            To whom:
          </label>
          <select
            className="form-select"
            aria-label="default select example"
            value={targetAuthor}
            name="targetAuthor"
            id="targetAuthor"
            onChange={(e) => setTargetAuthor(e.target.value)}
          >
            <option defaultValue={null}>Tag Someone</option>
            {userArray?.map((u) => {
              return (
                <option key={u.id} value={u.id}>
                  {u.name}
                </option>
              );
            })}
          </select>
        </div>
        <button type="submit" className="btn btn-success" data-bs-dismiss="modal">
          Submit
        </button>
      </form>
    </div>
  );
};
export default CreatePost;
