import { useState } from "react"
import { updateUser } from "../../api/userApi";
import keycloak from "../../keycloak/keycloak";
import './EditUser.css'

const EditUser = ({user}) => {
    const [updatedUser, setUpdatedUser] = useState({
        id: user?.id,
        name: user?.name,
        email: "",
        status: user?.status,
        picture: user?.picture,
        bio: user?.bio,
        funFact: user?.funFact
    });

    const changeEmail = () => {
        keycloak.accountManagement()
    }

    const handleInputChange = (event) => {
        event.preventDefault()
        const {name, value} = event.target
        
        setUpdatedUser({
            ...updatedUser,
            [name]: value,
            email: keycloak.tokenParsed.email
        })
        
    }
    
    const handleSubmit = () => {
        updateUser(updatedUser)
    }

    return (
          <div id="profile-edit" className="card card-body mb-5">
             <h2 className="mb-5">Edit User Information</h2>
             <button className="btn btn-info" onClick={changeEmail}>Change Email</button>
             <form onSubmit={handleSubmit}>
                <div className="form-group mt-3">
                    <label htmlFor="name">Name</label>
                    <input 
                        type="text" 
                        name="name" 
                        value={updatedUser.name || ""} 
                        onChange={handleInputChange} 
                        className="form-control" 
                        id="user-name" 
                        aria-describedby="user-name-desc" 
                        placeholder={user?.name}/>
                    <small id="user-name-desc" className="form-text text-muted">It is not recommended to use a nickname</small>
                </div>
                <div className="form-group mt-3">
                    <label htmlFor="status">Work Status</label>
                    <input 
                        type="text" 
                        name="status"
                        value={updatedUser.status || ""} 
                        onChange={handleInputChange} 
                        className="form-control" 
                        id="user-status" 
                        placeholder={user?.status}/>
                </div>
                <div className="form-group mt-3">
                    <label htmlFor="picture">Picture Link</label>
                    <input 
                        type="text" 
                        name="picture" 
                        value={updatedUser.picture || ""} 
                        onChange={handleInputChange} 
                        className="form-control" 
                        id="user-picture" 
                        aria-describedby="user-picture-desc" 
                        placeholder={user?.picture}/>
                    <small id="user-picture-desc" className="form-text text-muted">Enter a link to a picture here</small>
                </div>
                <div className="form-group mt-3">
                    <label htmlFor="bio">Biography</label>
                    <textarea 
                        type="textarea" 
                        name="bio"
                        value={updatedUser.bio || ""} 
                        onChange={handleInputChange} 
                        className="form-control pb-5" 
                        id="user-bio" 
                        placeholder={user?.bio}/>
                </div>
                <div className="form-group mt-3">
                    <label htmlFor="funFact">Fun Fact</label>
                    <input 
                        type="text" 
                        name="funFact"
                        value={updatedUser.funFact || ""} 
                        onChange={handleInputChange} 
                        className="form-control" 
                        id="user-funFact" 
                        placeholder={user?.funFact}/>
                </div>
                <input type="submit" value="Make Changes" className="btn btn-primary m-3" />
             </form>
          </div>
      )
  
  }
  
  export default EditUser