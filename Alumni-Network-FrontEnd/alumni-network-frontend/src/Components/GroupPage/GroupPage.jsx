import React from 'react'
import { getSpecificGroupById, getAllGroups, getAllGroupsNotPrivate,searchGroup, createGroup } from './../../api/groupApi';
import { useState, useEffect } from 'react';



const GroupPage = () => {

    const [groups, setGroups] = useState([]);
    const [search, setNewSearch] = useState("");
    const [showPrivate, setShowPrivate] = useState([]);
    const [postGroup, SetPostGroup] = useState({
        name: "",
        description: "",
        isPrivate: false
    });

    const [groupMemberPost, setShowGroupMemberPost] = useState([]);

  const handleSearchChange = (e) => {
    setNewSearch(e.target.value);
};

const handlePriveteGroups = () => {
  
 setShowPrivate( groups.map((group) => {
            if(group.isPrivate !== false)
             {
                return group;
             }    
    }).filter(g => g !== undefined))
}

const filtered = !search
    ? groups
    : groups.filter((group) =>
        group.name.toLowerCase().includes(search.toLowerCase())
      );

    useEffect(() => {
    
       getAllGroups().then(res => setGroups(res))
    
    }, [])


   
    

 const handleChange = event => {
    event.preventDefault();
    const { name, value } = event.target
    SetPostGroup({ 
    ...postGroup,
            [event.target.name]: value,
            isPrivate: event.target.checked
     });

  }

const handleSubmit = (event) => {
    event.preventDefault();

        const group = {
            name: postGroup.name,
            description: postGroup.description,
            isPrivate: postGroup.isPrivate
        }

        console.log(group)

       createGroup(1, group).then(res => res)
  }

    return (
    <>
 
<hr />
         <form onSubmit={handleSubmit}>
          <label>
            Name:
            <input type="text" name="name" value={postGroup.name} onChange={handleChange} />
          </label>
          <br />
          <label>
            Desc:
            <input type="text" name="description" value={postGroup.description} onChange={handleChange} />
          </label>
          <br />

           <label>
            Isprivate:
            <input type="checkbox" checked={postGroup.isPrivate} onChange={handleChange} />
          </label>
          <br />
          <button type="submit">Add Group</button>
        </form>


    <hr />
     <input type="text" value={search} onChange={handleSearchChange} />
         <h2>Search Groups</h2>
      {filtered.map((group) => {
        return (
          <p key={group.id}>
            {group.name} - {group.description}
          </p>
        );
      })}

        {groups.map(group => {
            return (
            <div className="card w-50 mb-2" key={group.id}>
            <div className="card-body">
                <h5 className="card-title">{group.name}</h5>
                <p className="card-text">{group.description}.</p>
                <a href="#" className="btn btn-primary">Button</a>
            </div>
        </div>
            )
        })}

        <button onClick={() => handlePriveteGroups()}>Show Private</button>
          {showPrivate.map(group => {
            return (
            <div className="card w-50 mb-2" key={group.id}>
            <div className="card-body">
                <h5 className="card-title">{group.name}</h5>
                <p className="card-text">{group.description}.</p>
                <a href="#" className="btn btn-primary">Button</a>
            </div>
        </div>
            )
        })}
    </>
  )
}

export default GroupPage