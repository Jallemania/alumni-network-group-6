import React, { useState, useEffect, useCallback } from 'react';
import { GetAllPostsFromGroup } from '../../api/apiPost';
import Dashboard from '../UserDashboardComponent/Dashboard';
import { JoinGroup } from './../../api/groupApi';


const Group = ({group, user}) => {
	const [member, setMember] = useState(true);
	
    const [groupPosts, setGroupPosts] = useState();
	const [g, setG] = useState(undefined);

	const handleGroupPosts = useCallback((taggedPosts) => {
        let posts = [];
            g?.posts.map(p => posts.push(p))
            taggedPosts?.map(p => !posts.includes(p) && posts.push(p))
            setGroupPosts(posts)
    }, [g])


	useEffect(() => {
		setG(group)
		
		if (g !== undefined) {
			GetAllPostsFromGroup(g?.id).then(res => handleGroupPosts(res));	
		}
	
		if (g?.users.some((u) => u.id === user.id)) {
			setMember(false);
		}
	}, [group, g, user.id, handleGroupPosts]);


	const joinTheGroup = async (gId, uId) => {
		setMember((prevState) => !prevState);

	 let groupIndex = g.findIndex(g => g.id === gId);
		await JoinGroup(gId, uId).then(res =>  g[groupIndex] = res);
	};

	return (
		<div className="d-flex flex-column  align-items-center w-100">
		
			<div className="col">
				<h1 className='text-uppercase'>{g?.name}</h1>
				<h4 className='font-italic'>{g?.description}</h4>

				<ul className='list-group'>
					{g?.users.map((members) => (
						<li className='list-group-item text-capitalize' key={members.id}>{members.name}</li>
					))}
				</ul>
			</div>
			<div className="col">
				<button
					className='btn btn-outline-success m-5'
					
					onClick={() => joinTheGroup(g.id, user.id)}
				>{`${member ? 'Join' : 'Already in'} Group`}</button>
			</div>

			<div className="col">
				<Dashboard posts={groupPosts} />
			</div>
		</div>
	);
};

export default Group;
