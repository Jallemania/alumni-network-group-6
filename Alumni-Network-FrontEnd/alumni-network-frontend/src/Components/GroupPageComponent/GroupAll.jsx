import React from 'react';
import { JoinGroup } from '../../api/groupApi';
import { leaveGroup } from './../../api/groupApi';
import { Loader } from './Loader';

const GroupAll = ({ groups, user, updatedom, isPending, handleModal }) => {


	const joinTheGroup = async (gId, uId) => {
		let groupIndex = groups.findIndex((g) => g.id === gId);
		await JoinGroup(gId, uId).then((res) => (groups[groupIndex] = res));
		updatedom();
	};

	const leaveTheGroup = async (gId, uId) => {
		let groupIndex = groups.findIndex((g) => g.id === gId);
		await leaveGroup(gId, uId).then((res) => (groups[groupIndex] = res));
		updatedom();
	};

	const isMember = (group) => {
		if (!group.users.some((u) => u.id === user.id)) {
			return true;
		} else {
			return false;
		}
	};

	return (
		<>
		<div className='card card-body' id="group-container">
		{isPending && <Loader />}
			<div className="row" >
				{groups?.map((group) => {
					return (
						<div className="col-sm-4 mb-4" key={group?.id}>
							<div className="card w-100  h-100 mb-2">
								<div className="card-body">
									<div className='d-flex justify-content-end mb-3'>
										<span className={group?.users.some((u) => u.id === user.id)
											? 'text-success '
											: ' text-danger '}>
										{group?.users.some((u) => u.id === user.id)
											? 'Member of '
											: ' Not Member of '}
									</span>
									<span className={group?.isPrivate ? 'badge bg-danger ms-2' : 'badge bg-success ms-2'}>
										{group?.isPrivate ? 'Private Group' : 'Public Group'}
									</span>
									</div>
									
									<h5 className="card-title">{group?.name}</h5>
									<p className="card-text">{group?.description}.</p>
									<button
										onClick={() => handleModal(group) }
										className="btn btn-outline-primary m-2"
										data-bs-toggle="modal"
            							data-bs-target="#myModal"
									>
										See Group
									</button>
									{isMember(group) ? (
										<button
											type="button"
											className="btn btn-outline-success m-2"
											onClick={() => joinTheGroup(group.id, user.id)}
										>
											Join Group
										</button>
									) : (
										<button
											type="button"
											className="btn btn-outline-danger m-2"
											onClick={() => leaveTheGroup(group.id, user.id)}
										>
											Leave Group
										</button>
									)}
								</div>
							</div>
						</div>
					);
				})}
			</div>	
		</div>
		</>
	);
};

export default GroupAll;