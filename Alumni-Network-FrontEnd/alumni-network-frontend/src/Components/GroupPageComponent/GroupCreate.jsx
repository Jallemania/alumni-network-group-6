

const GroupCreate = ({handleSubmit, name, setName, description, setDescription, isPrivate, setIsPrivate, message}) => {


  return (
    <>
     <h1> Add Group</h1>
         <form onSubmit={handleSubmit} >
          <div className="form-outline mb-3">
          <label className="form-label"> Name:</label>
          <input type="text" name="name" value={name}  className="form-control" placeholder='Name...' onChange={(e) => setName(e.target.value)} />
          </div>

          <div className="form-outline mb-3">
           <label className='form-label'>Description: </label>
            <textarea type="text" name="description" value={description}  className="form-control" onChange={(e) => setDescription(e.target.value)} />
          </div>

         <div className="mb-3 form-check">
          <label className='form-label'>
            Is Private?
            <input type="checkbox" className="form-check-input" value={isPrivate} checked={isPrivate} onChange={(e) => setIsPrivate(e.target.value)}  />
          </label>
         </div>
          <button type="submit" className="btn btn-primary btn-block mb-4" >Add Group</button>
          <div className="message">{message ? <p>{message}</p> : null}</div>
        </form>
    </>
  )
}

export default GroupCreate