import React from 'react';
import { getAllGroups, createGroup } from './../../api/groupApi';
import { useState, useEffect } from 'react';
import GroupCreate from './GroupCreate';
import GroupSearch from './GroupSearch';
import GroupAll from './GroupAll';
import { getUserByMail } from '../../api/userApi';
import keycloak from '../../keycloak/keycloak';
import Modal from '../Modal';
import Group from './Group';
import './GroupPage.css'
const GroupPage = () => {
	const [groups, setGroups] = useState(undefined);
	const [user, setUser] = useState(undefined);
	const [search, setNewSearch] = useState('');
	const [tab, setTab] = useState('groupAll');
	const [isPending, setIsPending] = useState(true);
	const [child, setChild] = useState()
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [isPrivate, setIsPrivate] = useState(false);
	const [message, setMessage] = useState('');

	useEffect(() => {
		if (user === undefined) {
			getUserByMail(keycloak.tokenParsed.email).then((res) => setUser(res));
		}
		if (groups === undefined) {
			getGroups();
		}
	}, [groups, user]);

	const handleModal = (theGroup) => {
		setChild(<Group user={user} group={theGroup}/>)
	};


	const tabs = (
			<nav className='navbar navbar-expand-lg'>
			<div className='container-fluid justify-content-around'>
			<ul className="d-flex list-unstyled">
			<li className="nav-item m-4  text-uppercase hover-underline-animation" onClick={() => setTab('groupAll')}>
				Groups
			</li>
			<li className="nav-item m-4 text-uppercase hover-underline-animation" onClick={() => setTab('groupSearch')}>
				Search
			</li>
			<li className="nav-item m-4 text-uppercase hover-underline-animation" onClick={() => setTab('groupCreate')}>
				Add Group
			</li>
		</ul>
			</div>
			</nav>
	);

	const getGroups =  () => {
		 getAllGroups().then((res) => {
			setGroups(res);
			setIsPending(false);
		});
	};

	const handleSearchChange = (e) => {
		setNewSearch(e.target.value);
	};

	const filtered = !search
		? groups
		: groups.filter((group) => {
				if (group.name !== null && group.name !== 'undefined') {
					console.log(group.name);
					return group.name.toLowerCase().includes(search.toLowerCase());
				}
				return null;
		  });

	const handleSubmit = (event) => {
		event.preventDefault();

		let group = {
			name: name,
			description: description,
			isPrivate: isPrivate,
		};

		createGroup(user.id, group).then((res) => {
			group = res;
			setName('');
			setDescription('');
			setIsPrivate('');
			setMessage('Group successfully created');
			getGroups();
		});
	
		
	};

	return (
		<>
			<div className="container-fluid">
				{tabs}
				{tab === 'groupAll' ? (
					<GroupAll
						groups={groups}
						user={user}
						updatedom={getGroups}
						isPending={isPending}
						setIsPending={setIsPending}
						handleModal={handleModal}
					/>
				) : tab === 'groupSearch' ? (
					<GroupSearch
						search={search}
						handleSearchChange={handleSearchChange}
						filtered={filtered}
						groups={groups}
							user={user}
							handleModal={handleModal}
					/>
				) : (
					tab === 'groupCreate' && (
						<GroupCreate
							handleSubmit={handleSubmit}
							
							setIsPending={setIsPending}
							isPending={isPending}
							updatedom={getGroups}
							groups={groups}
							setName={setName}
							name={name}
							setDescription={setDescription}
							description={description}
							isPrivate={isPrivate}
							setIsPrivate={setIsPrivate}
							message={message}
						/>
					)
				)}
			</div>
			<Modal children={child}/>
		</>
	);
};

export default GroupPage;
