import React from 'react';
import ".././TimelineComponent/Timeline.css"
const GroupSearch = ({ search, handleSearchChange, filtered, groups, user, handleModal }) => {

const isMember = (group) => {
		if (!group.users.some((u) => u.id === user.id)) {
			return true;
		} else {
			return false;
		}
	};
	return (
		<>
		
			<div className="container" id="timeline-container">
				<div className="row">
			<div className="input-group mb-3">
			<span className="input-group-text">
				Search Groups
			</span>
			<input
				className="form-control"
				type="text"
				value={search}
				onChange={handleSearchChange}
			/>
			</div>
			</div>

			{filtered.map((group) => {
				return (
					<div className="row">
				<div className="card w-100 h-100 mb-2">
								<div className="card-body">
									<div className='d-flex justify-content-end mb-3'>
										<span className={group?.users.some((u) => u.id === user.id)
											? 'text-success '
											: ' text-danger '}>
										{group?.users.some((u) => u.id === user.id)
											? 'Member of '
											: ' Not Member of '}
									</span>
									<span className={group?.isPrivate ? 'badge bg-danger ms-2' : 'badge bg-success ms-2'}>
										{group?.isPrivate ? 'Private Group' : 'Public Group'}
									</span>
									</div>
									
									<h3 className="card-title text-capitalize">{group?.name}</h3>
									<p className="card-text">{group?.description}.</p>
									<button
										onClick={() => handleModal(group) }
										className="btn btn-outline-primary m-2"
										data-bs-toggle="modal"
            							data-bs-target="#myModal"
									>
										See Group
									</button>
									
								</div>
							</div>
					
					
					</div>
				);
			})}
			
			
			
			</div>
		</>
	);
};

export default GroupSearch;
