
import React from 'react'

const GroupTimeLineItem = ({data}) => {
  return (
    <div className="timeline-item">
			<div className="timeline-item-content">
				<span className="tag"></span>
				<time>{data.timestamp}</time>
				<p>{data.postContent}</p>
				<p>{data.targetGroup.name}</p>
				<span className="circle" ></span>
			</div>
		</div>
  )
}

export default GroupTimeLineItem