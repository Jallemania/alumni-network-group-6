import React from 'react'
import { getSpecificGroupById } from '../../api/groupApi';
import { useState, useEffect } from 'react';
import GroupTimeLineItem from './GroupTimeLineItem';


const GroupTimeline = () => {
const [post, setPost] = useState([]);

 useEffect(() => {

   getSpecificGroupById(2)
   .then(res => setPost(res.posts))

 }, [])

    console.log(post)

  return (
     <>
     <h1>Hey</h1>
      <div className="timeline-container">
        {post.map((data, index) => 
        
            <GroupTimeLineItem data={data} key={index} />
        )}
        </div>
       </>
  )
}

export default GroupTimeline