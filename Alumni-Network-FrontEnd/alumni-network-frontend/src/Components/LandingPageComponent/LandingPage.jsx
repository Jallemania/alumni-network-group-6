import React from "react";
import keycloak from "../../keycloak/keycloak";
import "./LandingPage.css";
import { userExists, createUser } from "../../api/userApi";
import { useNavigate } from "react-router-dom";
import image from '../../images/logo.png'

const LandingPage = () => {
  const navigate = useNavigate();
  
  const checkUser = async () => {
    if (keycloak.authenticated) {
      const name = keycloak.tokenParsed.name;
      const email = keycloak.tokenParsed.email;

      if (await userExists(email)) {
        navigate('/profile');
      } else {
        createUser({ Name: name, Email: email });
        navigate("/profile");
      }
    }
  };

  checkUser();

  return (
    <div id="landing-page-container d-flex flex-column">
      <h1 className="font-monospace">Welcome to</h1>
      <div>
        <img src={image} alt="logo" />
      </div>
      {!keycloak.authenticated && (
        <button className="btn btn-success m-5 ps-5 pe-5" onClick={keycloak.login}>
          Login
        </button>
      )}
    </div>
  );
};

export default LandingPage;
