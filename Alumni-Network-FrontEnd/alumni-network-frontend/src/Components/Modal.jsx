const Modal = ({ children }) => {
  return (
    <div className="modal" id="myModal">
      <div className="modal-dialog modal-xl">
        <div className="modal-content align-items-center mt-5 p-3">{children}</div>
      </div>
    </div>
  );
};
export default Modal;
