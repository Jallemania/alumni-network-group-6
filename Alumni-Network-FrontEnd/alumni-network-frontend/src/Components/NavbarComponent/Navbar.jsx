import { Link } from "react-router-dom"
import keycloak from "../../keycloak/keycloak"
import './Navbar.css'
import img from "../../images/simple-logo.png"
import React from "react";
const Navbar = () => {

    return (
        <div id="navbar-container" className="shadow">
            <div className="m-3" >
                <img src={img} alt="logo" id="navbar-logo"></img>
            </div>
            {keycloak.tokenParsed &&
            (<ul id="navbar-list">
                <li className="navbar-items m-3">
                    <Link to="/timeline">Timeline</Link>
                </li>
                <li className="navbar-items m-3" >
                    <Link to="/groups">Groups</Link>
                </li>
                <li className="navbar-items m-3"  >
                    <Link to="/profile">Profile</Link>
                </li>
            </ul>
            )}
        </div>
    )
}
export default Navbar