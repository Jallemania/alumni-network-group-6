import { useState } from "react";
import React from "react";

import { putPost } from "../../api/apiPost";

const EditPost = ({ post }) => {
  const [newPost, setNewPost] = useState({
    id: post.id,
    postContent: "",
  });
 
  
  const handleChanges = (event) => {
    event.preventDefault();

    console.log(event.target.value);
    const { value } = event.target;
    setNewPost({
      ...newPost,
      id: post.id,
      postContent: value,
    });
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    
    const posts = {
      id: newPost.id,
      postContent: newPost.postContent,
    };
    console.log(posts);
    console.log(putPost(posts));
    putPost(posts).then((response) => response);
  };

  return (
    
    <div onSubmit={handleSubmit} className="Edit-Post-form" id="Edit-Post-form">
      <form>
        <label>Edit Post-content: </label>
        <br />
        <input
          type="text"
          onChange={(e) => handleChanges(e)}
          className="Edit-PostContent-Input"
        />
        <input
          type="submit"
          data-bs-toggle="collapse"
          data-bs-target="#Edit-Post-form"
          aria-expanded="false"
          aria-controls="Edit-Post-form"
        ></input>
      </form>
      
    </div>
  );
};
export default EditPost;
