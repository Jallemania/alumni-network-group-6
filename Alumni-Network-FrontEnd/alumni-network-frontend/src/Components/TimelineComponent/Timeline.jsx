import React from "react";
import { useEffect } from "react";
import { useCallback } from "react";
import { useState } from "react";
import { getPosts } from "../../api/apiPost";
import { getUsers } from "../../api/userApi";
import { getAllGroups } from "../../api/groupApi";
import Modal from "../Modal";
import UserPage from "../UserPageComponent/UserPage";
import "./Timeline.css";
import { getUserByMail } from "../../api/userApi";
import CreatePost from "../CreatePostComponent/CreatePost";
import keycloak from "../../keycloak/keycloak";
import EditPost from "./EditPost";

const Timeline = () => {
  const [postArray, setPostArray] = useState(undefined);
  const [openThread, setOpenThread] = useState(false);
  const [threadPosts, setThreadPosts] = useState([]); //reply-posts
  const [openPost, setOpenPost] = useState([]); //Target-posts
  const [userArray, setUserArray] = useState([]);
  const [search, setNewSearch] = useState("");
  const [noResult, setNoResult] = useState(false);
  const [currentUser, setCurrentUser] = useState();
  const [filtered, setFiltered] = useState([]);
  const [btnText, setBtnText] = useState("Show Thread");
  const [groupArray, setGroupArray] = useState(undefined);
  const [child, setChild] = useState();
  let darkPostStyle = "p-2 m-2 bg-secondary bg-gradient text-light card col card-body";
  let lightPostStyle = "p-2 m-2 bg-light bg-gradient text-dark card col card-body";
  let modalId = 0;

  const handleModal = (id, showCreate) => {
    modalId = id;

    if (showCreate) {
      setChild(<CreatePost users={userArray} groups={groupArray} handleToplevelPosts={handleToplevelPosts} postArray={postArray} setPostArray={setPostArray}/>);
    } else {
      setChild(<UserPage id={modalId} />);
    }
  };

  const handleSearchChange = (e) => {
    setNewSearch(e.target.value.toLowerCase());
    let topPosts = [];
    let results = [];
    postArray.map((post) => post.replyParentId === null && topPosts.push(post));

    let searchValue = e.target.value.toLowerCase();

    if (searchValue !== "") {
      results = topPosts.filter(
        (post) =>
          post.postContent.toLowerCase().includes(searchValue) ||
          post.author.toLowerCase().includes(searchValue)
      );

      setFiltered(results);

      if (results.length === 0) setNoResult(true);
      else setNoResult(false);
    } else {
      handleToplevelPosts();
    }
  };

  const correctUser = (authorId) => {
    for (var i = 0; i < userArray?.length; i++) {
      if (authorId === userArray[i].id) {
        return userArray[i].name;
      }
    }
  };

  const correctGroup = (groupId) => {
    for (var i = 0; i < groupArray?.length; i++) {
      if (groupId === groupArray[i].id) {
        return groupArray[i].name;
      }
    }
  };

  const handleThreadPosts = (postId) => {
    let replies = [];

    postArray.map(
      (post) => post.replyParentId === postId && replies.push(post)
    );

    for (let i = 0; i < postArray.length; i++) {
      for (let j = 0; j < replies.length; j++) {
        if (postArray[i].replyParentId === replies[j].id) {
          replies.push(postArray[i]);
        }
      }
    }

    setThreadPosts(replies);

    if (openThread) {
      setOpenPost(0);
      setOpenThread(false);
      setThreadPosts([]);
      setBtnText("Show Thread");
    } else {
      setOpenPost(postId);
      setOpenThread(true);
      setBtnText("Close Thread");
    }
  };

  const handleToplevelPosts = useCallback(() => {
    let topPosts = [];

    postArray.map((post) => post.replyParentId === null && topPosts.push(post));
    postArray.sort((a, b) => a.timestamp.localeCompare(b.timestamp));

    setFiltered(topPosts.reverse());
  }, [postArray]);

  useEffect(() => {
    if (userArray.length === 0) {
      getUsers().then((result) => setUserArray(result));
    }

    if (groupArray === undefined) {
      getAllGroups().then((result) => setGroupArray(result));
    }

    if (postArray === undefined) {
      getPosts().then((result) => setPostArray(result));
    }

    if (postArray !== undefined) {
      handleToplevelPosts();
    }
  }, [userArray, postArray, threadPosts, groupArray, handleToplevelPosts]);
  useEffect(() => {
    getUserByMail(keycloak.tokenParsed.email).then((result) => setCurrentUser(result));
  }, [] );

  const threadList = threadPosts?.map((post) => (
    <div
      className="p-2 m-2 bg-dark bg-gradient text-white card col card-body"
      key={post.id}
    >
      <p>{correctUser(post.authorId)} </p>
      <p>{post.postContent}</p>
      <p>{post.timestamp.substr(0, 10)}</p>
      {post.targetGroupId && (
        <p>
          <b>Posted in:</b> {correctGroup(post.targetGroupId)}
        </p>
      )}
      {post.targetUserId && (
        <p>
          <b>Sent to:</b> {correctUser(post.targetUserId)}
        </p>
      )}
    </div>
  ));

  const topPostList = filtered?.map((post) => {
    post.author = correctUser(post.authorId);
    return (
      <div
        className={openPost === post.id ? lightPostStyle : darkPostStyle}
        key={post.id}
      >
        <h5
          className="clickable"
          onClick={() => handleModal(post.authorId, false)}
          data-bs-toggle="modal"
          data-bs-target="#myModal"
        >
          {post.author}
        </h5>
        <div>
          <p>{post.postContent}</p>
          {post.targetGroupId && (
            <p>
              <b>Posted in:</b> {correctGroup(post.targetGroupId)}
            </p>
          )}
          {post.targetUserId && (
            <p>
              <b>Sent to:</b> {correctUser(post.targetUserId)}
            </p>
          )}
          <p>{post.timestamp.substr(0, 10)}</p>
          <button
            className="btn btn-light"
            onClick={() => handleThreadPosts(post.id)}
          >
            {btnText}
          </button>
          {currentUser !== undefined && post.authorId === currentUser.id  &&
     <> <button className="btn btn-info m-2" type="button"  data-bs-toggle="collapse" data-bs-target={"#post-edit" + post.authorId + "" + post.id} aria-expanded="false" aria-controls={"post-edit" + post.authorId + "" + post.id}>Edit</button>
      <div className="collapse" id={"post-edit" + post.authorId + "" + post.id} >
      {post && <EditPost post={post}/> }
     
      </div></> }
        </div>
      </div>
    );
  });

  return (
    <div className="container" id="timeline-container">
      <div className="row">
        <div className="input-group mb-3">
          <span className="input-group-text" id="search-addon">
            Search Posts
          </span>
          <input
            type="text"
            className="form-control"
            value={search}
            onChange={handleSearchChange}
            aria-label="Search"
            aria-describedby="search-addon"
          />
        </div>
      </div>
      <div className="row">
        <div className={openThread ? "float-start col-8" : "float-start col"}>
          <h3>Post Timeline</h3>
          {topPostList.length > 0 ? (
            topPostList
          ) : noResult ? (
            <h4 className="mt-5 pt-5">No Results...</h4>
          ) : (
            <h4 className="mt-5 pt-5">Loading...</h4>
          )}
        </div>
        {openThread && (
          <div className="float-start ml-1 col-4 position-fixed end-0 shadow-lg p-3 mb-5 mt-4 me-4 bg-body rounded">
            <h3>Post Thread</h3>
            {threadList.length === 0 ? <h4>Empty...</h4> : threadList}
          </div>
        )}
        <div className="mb-5 mt-4 ms-5" id="create-button">
          <button
            className="btn btn-primary fs-3 ms-5 p-3"
            onClick={() => handleModal(0, true)}
            data-bs-toggle="modal"
            data-bs-target="#myModal"
          >
            Write New Post +
          </button>
        </div>
      </div>
      <Modal children={child} />
    </div>
  );
};
export default Timeline;
