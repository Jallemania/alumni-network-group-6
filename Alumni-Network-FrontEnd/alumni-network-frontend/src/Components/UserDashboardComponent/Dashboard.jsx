import React from "react";
import { useState, useEffect } from "react";
import { getAllGroups } from "../../api/groupApi";
import { getUsers } from "../../api/userApi";
import "./UserDashboard.css";

const Dashboard = ({ posts }) => {
  const [allPosts, setPosts] = useState();
  const [userArray, setUserArray] = useState();
  const [groupArray, setGroupArray] = useState();

  const correctUser = (authorId) => {
    for (var i = 0; i < userArray?.length; i++) {
      if (authorId === userArray[i].id) {
        return userArray[i].name;
      }
    }
  };

  const correctGroup = (groupId) => {
    for (var i = 0; i < groupArray?.length; i++) {
      if (groupId === groupArray[i].id) {
        return groupArray[i].name;
      }
    }
  };

  useEffect(() => {
    setPosts(posts?.reverse());

    if (userArray === undefined) {
      getUsers().then((res) => setUserArray(res));
    }

    if (groupArray === undefined) {
      getAllGroups().then((result) => setGroupArray(result));
    }
  }, [posts, userArray, groupArray]);

  const allPostsList = allPosts?.map((p) => {
    return (
      <div
        className="p-2 m-2 bg-secondary bg-gradient text-white card card-body"
        key={p.timestamp}
      >
        <h4>{correctUser(p.authorId)}</h4>
        <p>{p.postContent}</p>
        {p.targetGroupId && (
          <p>
            <b>Posted in:</b> {correctGroup(p.targetGroupId)}
          </p>
        )}
        {p.targetUserId && (
          <p>
            <b>Sent to:</b> {correctUser(p.targetUserId)}
          </p>
        )}
        <p>
          <small>{p.timestamp.substr(0, 10)}</small>
        </p>
      </div>
    );
  });

  return (
    <div id="dashboard" className="card card-body ps-5 pe-5 mb-5">
      <h1>Dashboard</h1>
      { allPosts?.length === 0 ? (
        <div className="fs-3">Empty...</div>
        ) : posts !== undefined ? (
        allPostsList
      ) : (
        <h4 className="pt-5 mt-5 mb-5">
          <b>Loading...</b>
        </h4>
      )}
    </div>
  );
};

export default Dashboard;
