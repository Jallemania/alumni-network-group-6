import keycloak from "../../keycloak/keycloak";
import "./ProfilePage.css";
import { getUserByMail, getUserById } from '../../api/userApi'
import React,{ useEffect, useState } from "react";
import Dashboard from "../UserDashboardComponent/Dashboard";
import EditUser from "../EditUserComponent/EditUser";
import UserGroups from "./UserGroups";
import { GetAllUserTaggedPosts } from "../../api/apiPost";
import { useCallback } from "react";
import { getSpecificGroupById } from "../../api/groupApi";
import Modal from "../Modal";
import Group from "../GroupPageComponent/Group";

const ProfilePage = () => {
    const [user, setUser] = useState();
    const [userPosts, setUserPosts] = useState();
    const [showDashboard, setShowDashboard] = useState(true);
	const [child, setChild] = useState()
    
    const handleModal = (theGroup) => {
		setChild(<Group user={user} group={theGroup}/>)
	};
    
    const handleGroups = useCallback(() => {
        let groupsTemp = [];
    
        user.groups.forEach((g) => {
        getSpecificGroupById(g.id).then((res) => groupsTemp.push(res));
        });
    
        user.groups = groupsTemp
        
      }, [user]);

    const handleUserPosts = useCallback((taggedPosts) => {
        let posts = [];
            user?.posts.map(p => posts.push(p))
            taggedPosts?.map(p => !posts.includes(p) && posts.push(p))
            setUserPosts(posts)
    }, [user])

    useEffect(() => {
        if(user === undefined){
            getUserByMail(keycloak.tokenParsed.email)
            .then(res => res !== undefined && getUserById(res.id).then(res => setUser(res)))            
        }
        if(user !== undefined){
            GetAllUserTaggedPosts(user.id).then(res => handleUserPosts(res))
            handleGroups()
        }
    }, [user, handleUserPosts, handleGroups])

  return (
        <div id="profile-page-container">
            <img src={user?.picture} alt="User" id="profile-picture" className="card card-body"></img>
            <h2>{user?.name}</h2>
            <button className="btn btn-outline-danger m-4 pe-4 ps-4" onClick={ ()=>{ keycloak.logout() } } >Logout</button>
            <p className="fs-5"><small><b>Fun Fact:</b> {user?.funFact}</small></p>
            <span className="fs-4"><b>Currently Employed at:</b> {user?.status}</span>
            <div className="card card-body w-100 m-4">
                <div className="fs-3">Biography</div>
                <p className="fs-5">{user?.bio}</p>
            </div>
            <button className="btn btn-info m-2" type="button" data-bs-toggle="collapse" data-bs-target="#user-edit" aria-expanded="false" aria-controls="user-edit">
                <b>Edit Profile</b>
            </button>
            <div className="collapse" id="user-edit">
                {user && <EditUser user={user}/>}
            </div>
            <br/>
            
            <div className="w-100">   
                <button className="btn btn-dark m-2 w-25" onClick={() => { setShowDashboard(true) }}>Dashboard</button>
                <button className="btn btn-light m-2 w-25" onClick={() => { setShowDashboard(false) }}>Joined Groups</button>              
                {showDashboard ? <Dashboard posts={userPosts} /> : <UserGroups groups={user.groups} user={user} handleModal={handleModal}/>}
            </div>
            <Modal children={child}/>
        </div>
    )

}

export default ProfilePage