import React, { useState } from "react";
import {  leaveGroup } from "./../../api/groupApi";
import { Loader } from "../GroupPageComponent/Loader";
import { useEffect } from "react";

const UserGroups = ({ groups, user, handleModal }) => {
  const [groupList, setGroupList] = useState(undefined);

  

  const leaveTheGroup = (gId, uId) => {
    let groupIndex = groups.findIndex((g) => g.id === gId);
     leaveGroup(gId, uId).then((res) => (groups[groupIndex] = res));
  };

  const isMember = (group) => {
    if (!group.users.some((u) => u.id === user.id)) {
      return true;
    } else {
      return false;
    }
  };

  useEffect(() => {
    if (groupList === undefined) {
        setGroupList(groups)
    }
}, [groupList, groups]);


  return (
    <>
      <div className="card card-body" id="group-container">
        <div className="row">
          <span className="fs-3 mb-2">Groups</span>
            {groupList === undefined && <Loader />}
          {groupList !== undefined &&
            groupList?.map((group) => {
              return (
                <div className="col mb-4" key={group?.id}>
                  <div className="card w-100 h-100 mb-2">
                    <div className="card-body">
                      <div className="d-flex justify-content-end mb-3">
                        <span
                          className={
                            group.users?.some((u) => u.id === user.id)
                              ? "text-success "
                              : " text-danger "
                          }
                        >
                          {group?.users.some((u) => u.id === user.id)
                            ? "Member of "
                            : " Not Member of "}
                        </span>
                        <span
                          className={
                            group?.isPrivate
                              ? "badge bg-danger ms-2"
                              : "badge bg-success ms-2"
                          }
                        >
                          {group?.isPrivate ? "Private Group" : "Public Group"}
                        </span>
                      </div>

                      <h5 className="card-title">{group?.name}</h5>
                      <p className="card-text">{group?.description}.</p>
                      <button
                        onClick={() => handleModal(group)}
                        className="btn btn-outline-primary m-2"
                        data-bs-toggle="modal"
                        data-bs-target="#myModal"
                      >
                        See Group
                      </button>
                      {isMember(group) && (
                        <button
                          type="button"
                          className="btn btn-outline-danger m-2"
                          onClick={() => leaveTheGroup(group.id, user.id)}
                        >
                          Leave Group
                        </button>
                      )}
                    </div>
                  </div>
                </div>
              );
            })}
        </div>
      </div>
    </>
  );
};

export default UserGroups;
