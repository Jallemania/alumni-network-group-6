import "./UserPage.css";
import { getUserById } from '../../api/userApi'
import React, { useState, useEffect, useCallback } from 'react'
import { GetAllUserTaggedPosts } from "../../api/apiPost";
import Dashboard from "../UserDashboardComponent/Dashboard";
import UserGroups from "./UserGroups";
import { getSpecificGroupById } from "../../api/groupApi";
import Group from "../GroupPageComponent/Group";
import Modal from "../Modal";

const UserPage = ({id}) => {
    const [user, setUser] = useState();
    const [userPosts, setUserPosts] = useState();
    const [showDashboard, setShowDashboard] = useState(true);
	const [child, setChild] = useState()

    const handleModal = (theGroup) => {
		setChild(<Group user={user} group={theGroup}/>)
	};
 
    const handleGroups = useCallback(() => {
        let groupsTemp = [];
    
        user.groups.forEach((g) => {
           getSpecificGroupById(g.id).then((res) => groupsTemp.push(res));
        });
    
        user.groups = groupsTemp
        
      }, [user]);

    const handleUserPosts = useCallback((taggedPosts) => {
        let posts = [];
            user?.posts.map(p => posts.push(p))
            taggedPosts?.map(p => !posts.includes(p) && posts.push(p))
            setUserPosts(posts)
    }, [user])

    useEffect(() => {
        if(user === undefined || user?.id !== id){
             getUserById(id).then(res => setUser(res))
        }
        if(user !== undefined){
            GetAllUserTaggedPosts(user.id).then(res => handleUserPosts(res))
            handleGroups()
        }
    }, [user, id, handleUserPosts, handleGroups])


  return (
        <div id="profile-page-container" className="d-flex flex-column justify-content-center w-100">
            <div className="p-2">
                <img src={user?.picture} alt="User" id="profile-picture" className="card card-body"></img>
            </div>
            <h2>{user?.name}</h2>
            <p className="fs-5"><small><b>Fun Fact:</b> {user?.funFact}</small></p>
            <span className="fs-4"><b>Currently Employed at:</b> {user?.status}</span>
            <div className="card card-body m-4">
                <div className="fs-3">Biography</div>
                <p className="fs-5">{user?.bio}</p>
            </div>
            <br/>
            <div className="d-flex flex-column justify-content-center align-items-center"> 
                <div className="w-100">
                    <button className="btn btn-dark m-2 w-25" onClick={() => { setShowDashboard(true) }}>Dashboard</button>
                    <button className="btn btn-light m-2 w-25" onClick={() => { setShowDashboard(false) }}>Joined Groups</button>               
                </div>  
                {showDashboard ? <Dashboard posts={userPosts}/> :  <UserGroups groups={user.groups} user={user} handleModal={handleModal} />}
            </div>
            <Modal children={child}/>
        </div>
    )

}

export default UserPage