import axios from ".";
// Get All Posts
export const getPosts = async () => {
    
    const postsURL = `https://alumninetwork6.azurewebsites.net/api/post`;
    var posts;
    await axios.get(postsURL)
    .then(res => posts = res.data)
    return posts
}
// Get Post
export const getPost = async (postId) => {
    const postsURL = `https://alumninetwork6.azurewebsites.net/api/post/${postId}`;
    var post;
    await axios.get(postsURL).then(res => post = res.data);
    return post;
}
// Create Post
export const PostPost = async (newPost) => {
    const postsURL = `https://alumninetwork6.azurewebsites.net/api/post`;
    
    let data;
    
    await axios.post(postsURL, newPost)
    .then(res => data = res.data)
    return data
}

// Update Post
export const putPost = async (updatedPost) => {
    const updateURL = `https://alumninetwork6.azurewebsites.net/api/post`;
   
    let data;
    
    await axios.put(updateURL, updatedPost)
    .then(res => data = res.data);

    return data;
}

// Post Exists
export const postExists = async (postId) => {
    const postURL = `https://alumninetwork6.azurewebsites.net/api/post/${postId}`;
    
    let bool
    
    await axios.get(postURL)
         .then(res => bool = res.data)
    return bool
}

export const GetAllUserPosts = async (userId) => {
    const postsURL = `https://alumninetwork6.azurewebsites.net/api/post/UserPost/${userId}`;
    var posts;
    await axios.get(postsURL)
    .then(res => posts = res.data)
    return posts
}

export const GetSubscribedGroupPosts = async (userId) => {
    const postsURL = `https://alumninetwork6.azurewebsites.net/api/post/GroupPost/${userId}`;
    var posts;
    await axios.get(postsURL)
    .then(res => posts = res.data)
    return posts
}

export const GetAllUserTaggedPosts = async (userId) => {
    const postsURL = `https://alumninetwork6.azurewebsites.net/api/post/post/user/${userId}`;
    var posts;
    await axios.get(postsURL)
    .then(res => posts = res.data)
    return posts;
}

export const GetAllPostsFromGroup = async (id) => {
    const postsURL = `https://alumninetwork6.azurewebsites.net/api/post/post/group/${id}`;
    var posts;
    await axios.get(postsURL)
    .then(res => posts = res.data)
    return posts
}