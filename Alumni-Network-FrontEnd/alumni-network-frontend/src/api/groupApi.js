import axios from ".";


export const getAllGroups = async () => {
const groupsUrl = `https://alumninetwork6.azurewebsites.net/api/group/allgroups`;
	var groups;
	await axios.get(groupsUrl)
		.then(res => groups = res.data)

		return groups;
};

export const getAllGroupsNotPrivate = async () => {
	const groupsUrl = `https://alumninetwork6.azurewebsites.net/api/group/getallgroupsnotprivate`;
	var groupsNotPrivate;
	await axios.get(groupsUrl)
		.then(res => groupsNotPrivate = res.data)
		return groupsNotPrivate;
};

//get specific group by id
export const getSpecificGroupById = async (groupId) => {
	const groupUrl = `https://alumninetwork6.azurewebsites.net/api/group/${groupId}`;
	var group;
	await axios.get(groupUrl)
		.then(res => group = res.data)
		return group;
};

//Create group
export const createGroup = async (userId, postGroup) => {
	const groupUrl = `https://alumninetwork6.azurewebsites.net/api/group/${userId}`;
	var group;
	 await axios.post(groupUrl, postGroup)
            .then(res =>  group = res.data)
			return group;
};

//Join Group
export const JoinGroup = async (groupId, userId) => {
	const groupUrl = `https://alumninetwork6.azurewebsites.net/api/group/${groupId}/join?userId=${userId}`;
	var group;
	 await axios.post(groupUrl, userId)
            .then(res =>  group = res.data)
			return group;
};
//Leave Group 
export const leaveGroup = async (groupId, userId) => {
const groupUrl = `https://alumninetwork6.azurewebsites.net/api/group/${groupId}/leave/${userId}`;
	var group;
	await axios.put(groupUrl, userId)
            .then(res =>  group = res.data)
			return group;
}

//Search Group
export const searchGroup = async (search) => {
	const groupUrl = `https://alumninetwork6.azurewebsites.net/api/group/groupname?groupName=${search}`;
	var group;
	axios.get(groupUrl, search)
            .then(res => group = res.data)

			return group;
};
