import axios from "axios";
import keycloak from "../keycloak/keycloak";

/**
 * 
 * @param {AxiosRequestHeaders} headers 
 * @param {Keycloak} keycloak 
 * @returns 
 */
const setAuthorizationHeaders = (headers, keycloak) => {

    const { token } = keycloak;
    return { 
        ...headers,
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`
    }
}

axios.interceptors.request.use(async (config) => {
    if (!keycloak.authenticated) {
        return config;
    }

    if (!keycloak.isTokenExpired()) {
        return {
            ...config,
            headers: setAuthorizationHeaders(config.headers, keycloak)
        }      
    }

    const HOUR_IN_SEC = 3600;

    try {
        await keycloak.updateToken(HOUR_IN_SEC);
    } catch (error) {
        console.log("Could not refresh token: Axios interceptor");
    }

    
    return {
        ...config,
        headers: setAuthorizationHeaders(config.headers, keycloak)
    }
})

export default axios