import axios from "./index";


    // Get All Users
     export const getUsers = async () => {
        
        const usersURL = `https://alumninetwork6.azurewebsites.net/api/user`;
        var users;
        
        await axios.get(usersURL)
            .then(res => users = res.data)
            return users
    }
    
    // Get User by ID
    export const getUserById = async (userId) => {
        const usersURL = `https://alumninetwork6.azurewebsites.net/api/user/${userId}`;
        var user  

        await axios.get(usersURL)
            .then(res => user = res.data);
        return user;

    }

    // Get User by Mail
    export const getUserByMail = async (email) => {
        const usersURL = `https://alumninetwork6.azurewebsites.net/api/user/mail/${email}`;
        var user  

        await axios.get(usersURL)
            .then(res => res.data)
            .then(data => user = data);
        return user;

    }
    
    // Create User
    export const createUser = async (newUser) => {
        const usersURL = `https://alumninetwork6.azurewebsites.net/api/user/create`;
        var createdUser

        axios.post(usersURL, newUser)
            .then(res => createdUser = res.data)

        return createdUser
    }
    
    // Update User
    export const updateUser = async (updatedUser) => {
        const updateURL = `https://alumninetwork6.azurewebsites.net/api/user/update`;
        var user

        await axios.put(updateURL, updatedUser)
            .then(res => user = res.data);

        return user
    }
    
    // User Exists
    export const userExists = async (userMail) => {
        const userURL = `https://alumninetwork6.azurewebsites.net/api/user/exists/${userMail}`;
        var bool;

         await axios.get(userURL)
            .then(res => bool = res.data)

        return bool;
    }

    

