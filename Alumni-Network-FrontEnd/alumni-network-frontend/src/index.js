import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { initialize } from './keycloak/keycloak';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <h1>Loading Keycloak Authorization...</h1>
)

initialize().then( () => {
  root.render(
    
  <React.StrictMode>
    <App />
  </React.StrictMode>
  );
}).catch( () => {
  root.render(
    <React.StrictMode>
      <h2>Keycloak Failed to Load</h2>
    </React.StrictMode>
  )
})

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
