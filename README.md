# Alumni-Network Case 

Our task was to create Alumni-Network portal, with purpose to ease communication between Noroff alumni and Noroff. This is a single page application that allows users to make posts, create and join groups, and maintain a profile page for other users to see. The application consists of three pages. One timeline page where all the posts are gathered in order, where users can see posts threads and also create new posts. Another page is the group page where the user can see all public groups, search and create new groups. Lastly, a user profile where the user can see any user related posts, edit the profile, and see which groups the user is a part of.

## Install

```
cd {destination-folder}
git clone https://gitlab.com/Jallemania/alumni-network-group-6.git
cd Alumni-Network-Frontend
npm install
```

### Dependencies
* Visual Studio
* Visual Studio Code
* Microsoft SQL Server Managment Studio 18
* Azure
* Windows 10
* SQL
* Keycloak
    - Authentication 
* C# 
* NET 6
* ASP.NET Core web API
    - AutoMapper.Extensions.Microsoft.DependencyInjection
    - Microsoft.AspNetCore.Authentication.JwtBearer
    - Microsoft.AspNetCore.Mvc.NewtonsoftJson
    - Microsoft.EntityFrameworkCore
    - Microsoft.EntityFrameworkCore.SqlServer
    - Microsoft.VisualStudio.Web.CodeGeneration.Design
    - Newtonsoft.Json
    - Swashbuckle.AspNetCore
* Swagger/Open API
* ReactJs
* Bootstrap 5

## Executing program

How to run the application

Clone repository and open with Visual Studio.
Insert your own server string as "DefaultConnection" in the appsettins.json file.


## Usage

--for development--
```
npm run dev
```

This will open a new Webpage in your browser at `localhost:3000`. Remember to use your React browser extention for easy access to component states

## Contributors 

[Elmin Nurkic (@gfunkmaster)](@gfunkmaster)
[Mattias Eriksson (@MattiasLeifEriksson)](@MattiasLeifEriksson)
[Marcus Jarlevid (@Jallemania)](@Jallemania)

## Contributing

No contributions allowed. We are the sheriff around these parts. 🤠

## Licence 

Copyright 2022, Noroff Accelerate AS

